# README #

This is new version of Tamil Historical Facts 4.0.10 (Yet to release) 
Running version is 4.0.9

### What is this purpose of this app? ###

* Showcase Tamil's rich history
* Explain the rich history to current generation
* Sharing old art and architecture to next generation

### What is this repository for? ###

* Complete code for Tamil Historical Facts
* Searching for a good company who can help us
* Also it is for study purpose.


### What is new in this version? ###

* Support for Android 7.0
* Share button bug resolved
* Supports 5 languages (Tamil, English, Hindi, Malayalam, Telugu)
* New Menus
* Image loads faster


### What will be in next version? ###

* New GCM integration
* Like / Unlike / Starred / Share count display
* Text size can be increased or decresed

### Where to get the latest version APK file? ###
* [Playstore link](https://play.google.com/store/apps/details?id=com.myhistory.android.ta)
* [Direct APK](https://bitbucket.org/NaagAlgates/historicalfacts_coreandroid_repo/raw/cb69df477d864243f31a6f204160c9cd6a36ed25/app/app-release.apk)

### Who do I talk to? ###

* Nagaraj - nagharaj.cse@gmail.com
* Venkatesh - venkatesh.mohanram@gmail.com

### Help us Translating in Different Languages ###
* [click here](https://crowdin.com/project/tamil-historical-facts)

### Donation ###
* Contact us if interested in donating