package com.myhistory.android.ta.gcm;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.text.TextUtils;
import android.util.Log;

import com.google.android.gms.iid.InstanceID;
import com.myhistory.android.ta.R;
import com.myhistory.android.ta.activity.MainActivity;
import com.myhistory.android.ta.activity.SplashActivity;
import com.myhistory.android.ta.async.sendRegistrationId;
import com.myhistory.android.ta.utility.GeneralHelper;
import com.myhistory.android.ta.utility.Helper;
import com.myhistory.android.ta.utility.Logger;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Nagaraj.A on 2/29/2016.
 * Last Modified by : Nagaraj.A
 * Package Name : com.myhistory.android.ta.gcm
 * Project Name : TamilHistoricalFacts
 */
public class GCMReceiver extends WakefulBroadcastReceiver {
    private static final String TAG = GCMReceiver.class.getSimpleName();
    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
    private static final String TOKEN_SCOPE = "GCM";

    @Override
    public void onReceive(final Context context, Intent intent) {
        Bundle bundle = intent.getExtras();
        final String message = bundle.getString("message");
        Log.v(TAG,simpleDateFormat.format(new Date()) + " Message is " + message);
        if(message!=null && message.length()>0) {
            SharedPreferences pref = context.getSharedPreferences(Helper.APP_SHARED_PREF, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = pref.edit();
            editor.putBoolean(Helper.PREF_HEAVY_LOAD, true);
            editor.apply();
            GeneralHelper.setBadge(context, 1);
            sendNotification(context, message);
        }
    }

    private void sendNotification(Context context, String msg) {
        try {
            NotificationManager mNotificationManager = (NotificationManager) context
                    .getSystemService(Context.NOTIFICATION_SERVICE);

            Intent intent = new Intent(context, SplashActivity.class);
            intent.putExtra("DISPLAY_MESSAGE", msg);
            Log.v("DISPLAY_MESSAGE",msg);

            PendingIntent contentIntent = PendingIntent.getActivity(context, 0, intent,PendingIntent.FLAG_UPDATE_CURRENT);
            //Resources res = context.getResources();
            //new Intent(context, DisplayActionEvent.class), 0);

            //contentIntent.getActivity(context, 0,intent, 0);

            /*NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
                    context).setSmallIcon(R.mipmap.ic_launcher)
                    .setContentTitle(context.getString(R.string.app_name))
                    .setStyle(new NotificationCompat.BigTextStyle().bigText(msg))
                    .setAutoCancel(true).setContentText(msg);

            mBuilder.setContentIntent(contentIntent);
            mNotificationManager.notify(1, mBuilder.build());*/

            Notification notification = new Notification.Builder(context)
                    .setAutoCancel(true)
                    .setContentIntent(contentIntent)
                    .setContentTitle(context.getString(R.string.app_name))
                    .setContentText(msg)
                    .setSmallIcon(getNotificationIcon())
                    .setDefaults(Notification.DEFAULT_ALL)
                    .build();

            mNotificationManager.notify(1,notification);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private int getNotificationIcon() {
        boolean useWhiteIcon = (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP);
        return useWhiteIcon ? R.drawable.ic_stat_notification : R.mipmap.ic_launcher;
    }

    public static boolean fetchDeviceToken(final Context context, boolean forced, final Runnable runnable) {
        SharedPreferences pref = context.getSharedPreferences(Helper.APP_SHARED_PREF, Context.MODE_PRIVATE);
        final String storedToken = pref.getString(Helper.GCM_REG_ID, null);
        Logger.v(TAG+" storedToken : ",storedToken);
        boolean doFetch = TextUtils.isEmpty(storedToken) || forced;
        if (!doFetch) {
            return false;
        }

        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {
                try {
                    InstanceID iid = InstanceID.getInstance(context);
                    String token = iid.getToken(Helper.DEFAULT_GCM_SENDER_ID, TOKEN_SCOPE);
                    Log.v(TAG, "Device registered with GCM, token = " + token
                            + " projectNumber: " + Helper.DEFAULT_GCM_SENDER_ID);
                    return token;
                } catch (IOException ex) {
                    Log.v(TAG, "Device failed to register with GCM");
                    return null;
                }
            }

            @Override
            protected void onPostExecute(String token) {
                if (!TextUtils.isEmpty(token) && !token.equals(storedToken)) {
                    //sendRegistrationId(token).execute();
                    new sendRegistrationId(context,token).execute();
                }

                if (runnable != null) {
                    runnable.run();
                }
            }
        }.execute();
        return true;
    }
}
