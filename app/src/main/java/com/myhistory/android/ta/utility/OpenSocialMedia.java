package com.myhistory.android.ta.utility;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;

/**
 * Created by Nagaraj on 05-11-2014.
 * Last Modified by : Nagaraj
 * Package Name : om.myhistory.android.ta.utility
 * Project Name : TamilHistoricalFacts
 */
public class OpenSocialMedia {
    Context activity;
    public OpenSocialMedia(Context context) {
        this.activity = context;
    }
    public Intent openFacebook(Context c,String fbID )
    {

        try
        {
            c.getPackageManager()
                    .getPackageInfo("com.facebook.katana", 0); //Checks if FB is even installed.
            return new Intent(Intent.ACTION_VIEW,
                    Uri.parse("fb://profile/"+fbID)); //Trys to make intent with FB's URI
        }
        catch (Exception e)
        {
            return new Intent(Intent.ACTION_VIEW,
                    Uri.parse("https://www.facebook.com/"+fbID)); //catches and opens a url to the desired page
        }

    }
    public void openLinkedIn(String LinkedInID, String LinkedinURL)
    {
        try
        {
            Intent linkedinIntent = new Intent(Intent.ACTION_VIEW);
            linkedinIntent.setClassName("com.linkedin.android", "com.linkedin.android.profile.ViewProfileActivity");
            linkedinIntent.putExtra("memberId", LinkedInID);
            activity.startActivity(linkedinIntent);
        }
        catch (Exception e)
        {
            openWebPage(LinkedinURL);
        }
    }

    public void openWebPage(String Url)
    {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(Url));
        activity.startActivity(browserIntent);
    }
    public void openGPlus(String profile)
    {
        try
        {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setClassName("com.google.android.apps.plus",
                    "com.google.android.apps.plus.phone.UrlGatewayActivity");
            intent.putExtra("customAppUri", profile);
            activity.startActivity(intent);
        }
        catch(ActivityNotFoundException e)
        {
            openWebPage("https://plus.google.com/"+profile+"/posts");
        }
    }
    public void openTwitter(String twtrName)
    {
        try
        {
            activity.startActivity(new Intent(Intent.ACTION_VIEW,Uri.parse("twitter://user?screen_name=" + twtrName)));
        }
        catch (ActivityNotFoundException e)
        {
            activity.startActivity(new Intent(Intent.ACTION_VIEW,Uri.parse("https://twitter.com/#!/" + twtrName)));
        }
    }
}
