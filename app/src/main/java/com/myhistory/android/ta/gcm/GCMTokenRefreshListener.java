package com.myhistory.android.ta.gcm;

import com.google.android.gms.iid.InstanceIDListenerService;

/**
 * Created by Nagaraj.A on 2/29/2016.
 * Last Modified by : Nagaraj.A
 * Package Name : com.myhistory.android.ta.gcm
 * Project Name : TamilHistoricalFacts
 */
public class GCMTokenRefreshListener extends InstanceIDListenerService {

    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();
        GCMReceiver.fetchDeviceToken(this, true, null);
    }
}
