package com.myhistory.android.ta.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.myhistory.android.ta.R;
import com.myhistory.android.ta.bean.DisplayStoryDetails;
import com.myhistory.android.ta.utility.GeneralHelper;
import com.myhistory.android.ta.utility.Helper;
import com.squareup.picasso.Picasso;


import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by Nagaraj.A on 9/2/2015.
 * Last Modified by : Nagaraj.A
 * Package Name : com.myhistory.android.ta.adapter
 * Project Name : TamilHistoricalFacts
 */
public class DisplayStoryAdapter extends BaseAdapter {
    private String TAG = "DisplayStoryAdapter";
    private List<DisplayStoryDetails> detailsList;
    private Context nContext;
    private LayoutInflater inflater;
    private static final int TYPE_ITEM1 = 0;
    private static final int TYPE_ITEM2 = 1;
    private ViewHolder holder;

    public DisplayStoryAdapter(Context c, List<DisplayStoryDetails> detailsList ){
        this.nContext = c;
        this.detailsList=detailsList;
    }

    @Override
    public Object getItem(int position) {
        Log.i(TAG, String.valueOf(detailsList.get(position)));
        return detailsList.get(position);
    }

    @Override
    public long getItemId(int position) {
        Log.i(TAG,"getItemId");
        return position;
    }
    @Override
    public View getView(int iListPosition, View convertView, ViewGroup parent) {
        Log.i(TAG, "getView");
        int type = getItemViewType(iListPosition);
        int dayDiff = (int) GeneralHelper.getDateDifference(detailsList.get(iListPosition).getInserted_datetime());
        String displayDate;// = nContext.getString(R.string.publishedDate);
        switch (dayDiff) {
            case 0:
                displayDate = nContext.getString(R.string.today);
                break;
            case 1:
                displayDate = nContext.getString(R.string.yesterday);
                break;
            default:
                displayDate = nContext.getString(R.string.publishedDate);
                displayDate = displayDate.replace("DAY_COUNT", "" + dayDiff);
                break;
        }
        String imgLoadURL = Helper.imgDefaultURL + detailsList.get(iListPosition).getStory_id() + ".jpg";
        Log.v(TAG, imgLoadURL);
        if (convertView == null) {
            inflater = (LayoutInflater) this.nContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            holder = new ViewHolder();
            if (type == TYPE_ITEM1) {
                convertView = inflater.inflate(R.layout.single_item_first_row, parent, false);
            }else {
                convertView = inflater.inflate(R.layout.single_item_story, parent, false);
            }
            convertView.setTag(holder);
        } else{
            holder =   (ViewHolder) convertView.getTag();
        }

        if (type == TYPE_ITEM1) {
            //holder.showNewIcon = (ImageView) convertView.findViewById(R.id.showNewIcon);
            holder.storyFirstRowImage=(ImageView) convertView.findViewById(R.id.storyFirstRowImage);
            holder.storyFirstRowTitle = (TextView) convertView.findViewById(R.id.storyFirstRowTitle);
            holder.storyFirstRowPublishedDate = (TextView) convertView.findViewById(R.id.storyFirstRowPublishedDate);
            //holder.storyFirstRowProgressBar = (ProgressBar) convertView.findViewById(R.id.progressbar);

            if (detailsList.size() > 0) {
                /*if(detailsList.get(iListPosition).getRead_status().equalsIgnoreCase("0"))
                    holder.showNewIcon.setVisibility(View.VISIBLE);
                else
                    holder.showNewIcon.setVisibility(View.GONE);*/
                holder.storyFirstRowTitle.setText(detailsList.get(iListPosition).getTam_title());
                holder.storyFirstRowPublishedDate.setText(displayDate);
                Picasso.with(nContext)
                        .load(imgLoadURL)
                        .placeholder(R.drawable.placeholder)
                        .fit()
                        .error(R.drawable.pageload_error)
                        .fit()
                        .into(holder.storyFirstRowImage);/*, new Callback() {
                                @Override
                                public void onError() {
                                    //holder.displayPic.setVisibility(View.VISIBLE);
                                    holder.storyFirstRowProgressBar.setVisibility(View.VISIBLE);
                                }

                                @Override
                                public void onSuccess() {
                                    holder.storyFirstRowImage.setVisibility(View.VISIBLE);
                                    holder.storyFirstRowProgressBar.setVisibility(View.GONE);
                                }
                            });*/
            }
        }else{// (type == TYPE_ITEM2) {

            holder.storyTitle = (TextView) convertView.findViewById(R.id.storyTitle);
            holder.storyPublishedDate = (TextView) convertView.findViewById(R.id.storyPublishedDate);
            holder.displayPic = (ImageView) convertView.findViewById(R.id.displayPic);
            //holder.progressbar = (ProgressBar) convertView.findViewById(R.id.progressbar);
            holder.storyDescription = (TextView) convertView.findViewById(R.id.storyDescription);
            holder.dayCount = (LinearLayout) convertView.findViewById(R.id.dayCount);
            holder.dayCount.bringToFront();

            if (detailsList.size() > 0) {
                holder.storyTitle.setText(detailsList.get(iListPosition).getTam_title());
                String sContent = detailsList.get(iListPosition).getTam_content();
                /*String sActualContent;
                if(sContent.length()>40) {
                    sActualContent = sContent.substring(0, 40)+ nContext.getText(R.string.dots);
                    //holder.storyDescription.setText(sActualContent );
                }
                else {
                    sActualContent = sContent.substring(0, sContent.length() - 5) + nContext.getText(R.string.dots);
                }*/
                holder.storyDescription.setText(sContent);
                Picasso.with(nContext)
                        .load(imgLoadURL)
                        .placeholder(R.drawable.placeholder)
                        .fit()
                        .error(R.drawable.pageload_error)
                        .fit()
                        .into(holder.displayPic);/*, new Callback() {
                            @Override
                            public void onError() {
                                //holder.displayPic.setVisibility(View.VISIBLE);
                                holder.progressbar.setVisibility(View.VISIBLE);
                            }

                            @Override
                            public void onSuccess() {
                                holder.displayPic.setVisibility(View.VISIBLE);
                                holder.progressbar.setVisibility(View.GONE);
                            }
                        });*/

                holder.storyPublishedDate.setText(displayDate);
            } else {
                Log.v(TAG, "Unusual Condition");
            }
        }

        return convertView;
    }
    @Override
    public int getCount() {
        return detailsList.size();
    }

    private static class ViewHolder {
        private TextView storyTitle, storyPublishedDate,storyDescription,storyFirstRowTitle,storyFirstRowPublishedDate;//,storyRead,storySocialStatus;
        private ImageView displayPic,storyFirstRowImage;//,showNewIcon;
        private LinearLayout dayCount;
        //private ProgressBar progressbar,storyFirstRowProgressBar;
    }
    int type;
    @Override
    public int getItemViewType(int position) {

        if (position== 0){ // position 0
            type = TYPE_ITEM1;
        } else {
            type = TYPE_ITEM2;
        }

        return type;
        //return (position == this.getCount() - 1) ? 1 : 0;
    }
    @Override
    public int getViewTypeCount() {
        return 2; // we have 2 types
    }
}
