package com.myhistory.android.ta.fragment;

import android.Manifest;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.myhistory.android.ta.App;
import com.myhistory.android.ta.R;
import com.myhistory.android.ta.activity.ShowImageFullScreen;
import com.myhistory.android.ta.bean.DisplayStoryDetails;
import com.myhistory.android.ta.bean.StoryStatus;
import com.myhistory.android.ta.essentials.GPSTracker;
import com.myhistory.android.ta.essentials.TouchHighlightImageButton;
import com.myhistory.android.ta.utility.DatabaseMenuHandler;
import com.myhistory.android.ta.utility.GeneralHelper;
import com.myhistory.android.ta.utility.Helper;
import com.myhistory.android.ta.utility.Logger;
import com.squareup.picasso.Picasso;

import java.util.List;

import tourguide.tourguide.ChainTourGuide;
import tourguide.tourguide.Overlay;
import tourguide.tourguide.Pointer;
import tourguide.tourguide.Sequence;
import tourguide.tourguide.ToolTip;
import tourguide.tourguide.TourGuide;

import static android.text.Spanned.SPAN_EXCLUSIVE_EXCLUSIVE;

/**
 * Created by Nagaraj.A on 9/4/2015.
 * Last Modified by : Nagaraj.A
 * Package Name : com.myhistory.android.ta.fragment
 * Project Name : TamilHistoricalFacts
 */
public class ScreenSlidePageFragment extends Fragment  implements View.OnClickListener {
    View rootView;
    private static String TAG="ScreenSlidePageFragment";
    public static final String ARG_PAGE = TAG;
    private int mPageNumber;
    List<DisplayStoryDetails> detailsList;
    LinearLayout storyShare,storyMap,storyWriter,storyTranslate;
    //storyLike,storyUnlike,storyImportant,
    DatabaseMenuHandler databaseMenuHandler;
    TouchHighlightImageButton storyDisplayImg;
    int dataCount=0;
    Sequence sequence;


    public static ScreenSlidePageFragment create(int pageNumber) {
        ScreenSlidePageFragment fragment = new ScreenSlidePageFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_PAGE, pageNumber);
        fragment.setArguments(args);
        return fragment;
    }

   /* public ScreenSlidePageFragment() {
        *//*databaseMenuHandler = new DatabaseMenuHandler(getActivity());
        dataCount = databaseMenuHandler.getCountFromTable("SELECT COUNT(*) FROM TBL_STORY_DETAILS");
        databaseMenuHandler.close();*//*
    }*/

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPageNumber = getArguments().getInt(ARG_PAGE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_screen_slide_page, container, false);
        try {
        /*MainActivity activity = (MainActivity) getActivity();
        String sortType = activity.getSortType();
        int sortingFlag=0;
        try{
            sortingFlag = Integer.parseInt(sortType);
        }catch (Exception e){
            e.printStackTrace();
        }*/

            int sortingFlag = 0;
            try {
                getActivity();
                SharedPreferences prefs = getActivity().getSharedPreferences(Helper.APP_SHARED_PREF, Context.MODE_PRIVATE);
                sortingFlag = Integer.parseInt(prefs.getString(Helper.SORTING_TYPE, "0"));
            } catch (Exception e) {
                e.printStackTrace();
            }
            databaseMenuHandler = new DatabaseMenuHandler(getActivity());
            int pageNum = getPageNumber();
            switch (sortingFlag) {
                case 1:
                    //விரும்பியவை
                    dataCount = databaseMenuHandler.getCountFromTable("SELECT COUNT(*) FROM TBL_STORY_DETAILS WHERE social_status = 1");
                    //detailsList = databaseMenuHandler.storyDetailsList(pageNum+ 2, pageNum + 2, sortingFlag);
                    break;
                case 2:
                    //நட்சத்திரமிட்டவை
                    dataCount = databaseMenuHandler.getCountFromTable("SELECT COUNT(*) FROM TBL_STORY_DETAILS WHERE download_status = 1");
                    //detailsList = databaseMenuHandler.storyDetailsList(pageNum + 2, pageNum + 2, sortingFlag);
                    break;
                case 3:
                    //வழிசெலுத்தல் உள்ளவை
                    dataCount = databaseMenuHandler.getCountFromTable("SELECT COUNT(*) FROM TBL_STORY_DETAILS WHERE geo_location <> 0");
                    //detailsList = databaseMenuHandler.storyDetailsList(pageNum + 2, pageNum + 2, sortingFlag);
                    break;
                default:
                    //அனைத்தும்
                    dataCount = databaseMenuHandler.getCountFromTable("SELECT COUNT(*) FROM TBL_STORY_DETAILS");
                    //detailsList = databaseMenuHandler.storyDetailsList(dataCount - pageNum, dataCount -pageNum, sortingFlag);
                    break;
            }
            detailsList = databaseMenuHandler.storyDetailsList( dataCount-pageNum, dataCount-pageNum,sortingFlag);
            (rootView.findViewById(R.id.storyTitle)).bringToFront();
            ((TextView) rootView.findViewById(R.id.storyTitle)).setText(
                    getActivity().getString(R.string.storyTitleSetText, detailsList.get(0).getTam_title().trim()));
            String sContent = getActivity().getString(R.string.storyTitleSetText, detailsList.get(0).getTam_content().trim());
            /*final Spannable spannableString = new SpannableString(sContent);
            spannableString.setSpan(new RelativeSizeSpan(2.0f), 0, 1, SPAN_EXCLUSIVE_EXCLUSIVE);
            spannableString.setSpan(new ForegroundColorSpan(Color.RED), 0, 1, SPAN_EXCLUSIVE_EXCLUSIVE);*/

            ((TextView) rootView.findViewById(R.id.storyContent)).setText(String.format("%s%s", getString(R.string.tab), sContent));
            final String imgLoadURL = Helper.imgDefaultURL + detailsList.get(0).getStory_id() + ".jpg";
            Picasso.with(getActivity())
                    .load(imgLoadURL)
                    .placeholder(R.drawable.placeholder)
                    .fit()
                    .error(R.drawable.pageload_error)
                    .fit()
                    .into((TouchHighlightImageButton) rootView.findViewById(R.id.storyDisplayImage));
            //databaseMenuHandler = new DatabaseMenuHandler(getActivity());
            databaseMenuHandler.updateAsRead(detailsList.get(0).getStory_id());
            List<StoryStatus> storyStatusList = databaseMenuHandler.getStoryStatusList(detailsList.get(0).getStory_id());

            storyDisplayImg = (TouchHighlightImageButton) rootView.findViewById(R.id.storyDisplayImage);
            storyMap = (LinearLayout) rootView.findViewById(R.id.storyMap);
            storyWriter = (LinearLayout) rootView.findViewById(R.id.storyWriter);
            storyTranslate = (LinearLayout) rootView.findViewById(R.id.storyTranslate);
            /*storyLike = (ImageView) rootView.findViewById(R.id.storyLike);
            storyUnlike = (ImageView) rootView.findViewById(R.id.storyUnlike);
            storyImportant = (ImageView) rootView.findViewById(R.id.storyImportant);*/
            storyShare = (LinearLayout) rootView.findViewById(R.id.storyShare);


            /*if (storyStatusList.get(0).getSocialStatus().length() > 0) {
                switch (storyStatusList.get(0).getSocialStatus()) {
                    case "1":
                        storyUnlike.setImageResource(R.drawable.ic_action_bad);
                        storyLike.setImageResource(R.drawable.ic_action_good_selected);
                        break;
                    case "2":
                        storyUnlike.setImageResource(R.drawable.ic_action_bad_selected);
                        storyLike.setImageResource(R.drawable.ic_action_good);
                        break;
                    default:
                        storyUnlike.setImageResource(R.drawable.ic_action_bad);
                        storyLike.setImageResource(R.drawable.ic_action_good);
                        break;
                }
            }
            if (storyStatusList.get(0).getDownloadStatus().length() > 0) {
                switch (storyStatusList.get(0).getDownloadStatus()) {
                    case "1":
                        storyImportant.setImageResource(R.drawable.ic_action_important);
                        break;
                    default:
                        storyImportant.setImageResource(R.drawable.ic_action_not_important);
                        break;
                }
            }*/
            storyDisplayImg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //zoomImageFromThumb(storyDisplayImg, imgLoadURL);
                    Intent a = new Intent(getActivity(), ShowImageFullScreen.class);
                    a.putExtra("IMAGE_URL",imgLoadURL);
                    getActivity().startActivity(a);
                }
            });
            if (detailsList.get(0).getGeo_location().length() > 5) {
                storyMap.setVisibility(View.VISIBLE);
                storyMap.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //iconMapClicked();
                        showHelp("MAP");
                    }
                });
            } else
                storyMap.setVisibility(View.GONE);
            /*storyLike.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    iconLikeClicked();
                }
            });
            storyUnlike.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    iconUnLikeClicked();
                }
            });
            storyImportant.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    iconImportantClicked();
                }
            });*/
            storyShare.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //iconShareClicked();
                    showHelp("SHARE");
                }
            });
            storyWriter.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //iconUploadClicked();
                    showHelp("UPLOAD");
                }
            });
            storyTranslate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //iconTranslateClicked();
                    showHelp("TRANSLATE");
                }
            });

        }catch (Exception e){
            e.printStackTrace();
        }
        return rootView;
    }

    @Override
    public void onClick(View v) {
        /*if (v == rootView.findViewById(R.id.storyLike)) {
            //do something
            String storyID=detailsList.get(0).getStory_id();
            Toast.makeText(getActivity(),storyID,Toast.LENGTH_SHORT).show();
        }*/
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        Log.i(TAG, "onActivityCreated");
        super.onActivityCreated(savedInstanceState);
    }
    public int getPageNumber() {
        return mPageNumber;
    }

    private void iconMapClicked() {
        Log.v(TAG, detailsList.get(0).getGeo_location());
        if(detailsList.get(0).getGeo_location().split(",").length>0 && !detailsList.get(0).getGeo_location().equalsIgnoreCase("0")) {
            String[] geoLocation = detailsList.get(0).getGeo_location().split(",");
            if (geoLocation[0].trim().length() > 0 && !geoLocation[0].trim().equalsIgnoreCase("0")) {
                double siteLatitude = Double.valueOf(geoLocation[0].trim());
                double siteLongitude = Double.valueOf(geoLocation[1].trim());
                GPSTracker gps = new GPSTracker(getActivity().getApplicationContext());
                if (gps.canGetLocation()) {
                    double myLatitude = gps.getLatitude();
                    double myLongitude = gps.getLongitude();
                    Log.i("MyLATITUDE", String.valueOf(myLatitude));
                    Log.i("MyLONGITUDE", String.valueOf(myLongitude));
                    try {
                        Toast.makeText(getActivity(), getActivity().getString(R.string.mapOpening), Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                                Uri.parse("http://maps.google.com/maps?   saddr=" + myLatitude + "," + myLongitude + "&daddr=" + siteLatitude + "," + siteLongitude));
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.addCategory(Intent.CATEGORY_LAUNCHER);
                        intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
                        startActivity(intent);
                    } catch (ActivityNotFoundException e) {
                        Toast.makeText(getActivity().getApplicationContext(), "Google Map is not installed", Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(Intent.ACTION_VIEW).setData(Uri.parse("http://maps.google.com/maps?   saddr=" + myLatitude + "," + myLongitude + "&daddr=" + siteLatitude + "," + siteLongitude)));
                    } catch (Exception E) {
                        E.printStackTrace();
                    }

                } else {
                    gps.showSettingsAlert();
                }
            } else {
                Toast.makeText(getActivity(), getActivity().getString(R.string.navigationBlocked), Toast.LENGTH_SHORT).show();
            }
        }else {
            Toast.makeText(getActivity(), getActivity().getString(R.string.navigationBlocked), Toast.LENGTH_SHORT).show();
        }
    }
    /*private void iconLikeClicked() {
        storyUnlike.setImageResource(R.drawable.ic_action_bad);
        storyLike.setImageResource(R.drawable.ic_action_good_selected);
        //databaseMenuHandler = new DatabaseMenuHandler(getActivity());
        databaseMenuHandler.updateSocialStatus(detailsList.get(0).getStory_id(), "1");
        //databaseMenuHandler.close();
    }*/
    /*private void iconImportantClicked() {
        String sQuery="SELECT download_status FROM TBL_STORY_DETAILS WHERE story_id = "+detailsList.get(0).getStory_id();
        //databaseMenuHandler = new DatabaseMenuHandler(getActivity());
        int iDownloadStatus = databaseMenuHandler.getCountFromTable(sQuery);
        switch (iDownloadStatus){
            case 1:
                storyImportant.setImageResource(R.drawable.ic_action_not_important);
                databaseMenuHandler.updateDownloadStatus(detailsList.get(0).getStory_id(), "2");
                break;
            case 2:
                storyImportant.setImageResource(R.drawable.ic_action_important);
                databaseMenuHandler.updateDownloadStatus(detailsList.get(0).getStory_id(), "1");
                break;
            default:
                storyImportant.setImageResource(R.drawable.ic_action_important);
                databaseMenuHandler.updateDownloadStatus(detailsList.get(0).getStory_id(), "1");
                break;
        }
        //databaseMenuHandler.close();
    }
    private void iconUnLikeClicked() {
        storyLike.setImageResource(R.drawable.ic_action_good);
        storyUnlike.setImageResource(R.drawable.ic_action_bad_selected);
        databaseMenuHandler.updateSocialStatus(detailsList.get(0).getStory_id(), "2");
        //databaseMenuHandler = new DatabaseMenuHandler(getActivity());
        //databaseMenuHandler.close();
    }*/
    private void iconTranslateClicked(){
        Intent i = new Intent(Intent.ACTION_SENDTO);
        i.setData(Uri.parse("mailto:"));
        i.putExtra(Intent.EXTRA_EMAIL, Helper.contactMailID);
        i.putExtra(Intent.EXTRA_CC,  Helper._contactMailIDs);
        i.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.translateSubject) + " "+detailsList.get(0).getTam_title());
        i.putExtra(Intent.EXTRA_TEXT   , "");
        try {
            startActivity(Intent.createChooser(i, "Mail"));
        } catch (android.content.ActivityNotFoundException ex) {
            //getActivity().getApplicationContext().TshowShortToastMessage(mContext, R.string.msgNoMailClientsInstalled);
            Toast.makeText(App.get(),getString(R.string.msgNoMailClientsInstalled),Toast.LENGTH_LONG).show();
        }
    }
    private void iconUploadClicked(){
        Intent i = new Intent(Intent.ACTION_SENDTO);
        i.setData(Uri.parse("mailto:"));
        i.putExtra(Intent.EXTRA_EMAIL, Helper.contactMailID);
        i.putExtra(Intent.EXTRA_CC,  Helper._contactMailIDs);
        i.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.uploadArticleSubject));
        i.putExtra(Intent.EXTRA_TEXT   , "");
        try {
            startActivity(Intent.createChooser(i, "Mail"));
        } catch (android.content.ActivityNotFoundException ex) {
            //getActivity().getApplicationContext().TshowShortToastMessage(mContext, R.string.msgNoMailClientsInstalled);
            Toast.makeText(App.get(),getString(R.string.msgNoMailClientsInstalled),Toast.LENGTH_LONG).show();
        }
    }
    private void iconShareClicked() {
        GeneralHelper.verifyStoragePermissions(getActivity());
        Uri bmpUri = GeneralHelper.getLocalBitmapUri(storyDisplayImg);
        if(bmpUri!=null) {
            Intent i = new Intent(android.content.Intent.ACTION_SEND);
            i.setType("*/*");
            i.putExtra(android.content.Intent.EXTRA_SUBJECT, detailsList.get(0).getTam_title());
            i.putExtra(Intent.EXTRA_STREAM, bmpUri);
            i.putExtra(android.content.Intent.EXTRA_TEXT, "\nSimilar article on:" + getText(R.string.ShortURL) + "\n\n" + detailsList.get(0).getTam_content());
            startActivity(Intent.createChooser(i, getActivity().getString(R.string.share)));
        }else{
            Toast.makeText(getActivity(),getActivity().getString(R.string.internalServerError),Toast.LENGTH_SHORT).show();
        }
    }

    private void helpSequence(){

        Animation mEnterAnimation = new AlphaAnimation(0f, 1f);
        mEnterAnimation.setDuration(600);
        mEnterAnimation.setFillAfter(true);

        Animation mExitAnimation = new AlphaAnimation(1f, 0f);
        mExitAnimation.setDuration(600);
        mExitAnimation.setFillAfter(true);

        ChainTourGuide tourGuide1 = ChainTourGuide.init(getActivity())
                .setToolTip(new ToolTip()
                        .setTitle(getString(R.string.helpImageTitle))
                        .setDescription(getString(R.string.helpImageDescription))
                        .setGravity(Gravity.BOTTOM)
                )
                .setOverlay(new Overlay()
                        .setBackgroundColor(Color.parseColor("#EE2c3e50"))
                        .setEnterAnimation(mEnterAnimation)
                        .setExitAnimation(mExitAnimation)
                )
                // note that there is no Overlay here, so the default one will be used
                .playLater(storyDisplayImg);

        ChainTourGuide tourGuide2 = ChainTourGuide.init(getActivity())
                .setToolTip(new ToolTip()
                        .setTitle(getString(R.string.helpUploadTitle))
                        .setDescription(getString(R.string.helpUploadDescription))
                        .setGravity(Gravity.TOP)
                        //.setBackgroundColor(Color.parseColor("#c0392b"))
                )
                .setOverlay(new Overlay()
                        .setBackgroundColor(Color.parseColor("#EE2c3e50"))
                        .setEnterAnimation(mEnterAnimation)
                        .setExitAnimation(mExitAnimation)
                )
                .playLater(storyWriter);

        ChainTourGuide tourGuide3 = ChainTourGuide.init(getActivity())
                .setToolTip(new ToolTip()
                        .setTitle(getString(R.string.helpTranslateTitle))
                        .setDescription( getString(R.string.helpTranslateDescription))
                        .setGravity(Gravity.TOP)
                )
                .setOverlay(new Overlay()
                        .setBackgroundColor(Color.parseColor("#EE2c3e50"))
                        .setEnterAnimation(mEnterAnimation)
                        .setExitAnimation(mExitAnimation)
                )
                // note that there is no Overlay here, so the default one will be used
                .playLater(storyTranslate);

        ChainTourGuide tourGuide4 = ChainTourGuide.init(getActivity())
                .setToolTip(new ToolTip()
                        .setTitle(getString(R.string.helpMapTitle))
                        .setDescription(getString(R.string.helpMapDescription))
                        .setGravity(Gravity.TOP)
                )
                .setOverlay(new Overlay()
                        .setBackgroundColor(Color.parseColor("#EE2c3e50"))
                        .setEnterAnimation(mEnterAnimation)
                        .setExitAnimation(mExitAnimation)
                )
                // note that there is no Overlay here, so the default one will be used
                .playLater(storyMap);

        ChainTourGuide tourGuide5 = ChainTourGuide.init(getActivity())
                .setToolTip(new ToolTip()
                        .setTitle(getString(R.string.helpShareTitle))
                        .setDescription(getString(R.string.helpShareDescription))
                        .setGravity(Gravity.TOP)
                )
                .setOverlay(new Overlay()
                        .setBackgroundColor(Color.parseColor("#EE2c3e50"))
                        .setEnterAnimation(mEnterAnimation)
                        .setExitAnimation(mExitAnimation)
                )
                // note that there is no Overlay here, so the default one will be used
                .playLater(storyShare);

        if(storyMap.getVisibility()==View.GONE){
            sequence = new Sequence.SequenceBuilder()
                    .add(tourGuide1, tourGuide2, tourGuide3, tourGuide5)
                    .setDefaultOverlay(new Overlay()
                            .setEnterAnimation(mEnterAnimation)
                            .setExitAnimation(mExitAnimation)
                    )
                    .setDefaultPointer(null)
                    .setContinueMethod(Sequence.ContinueMethod.Overlay)
                    .build();
        }else {
            sequence = new Sequence.SequenceBuilder()
                    .add(tourGuide1, tourGuide2, tourGuide3, tourGuide4, tourGuide5)
                    .setDefaultOverlay(new Overlay()
                            .setEnterAnimation(mEnterAnimation)
                            .setExitAnimation(mExitAnimation)
                    )
                    .setDefaultPointer(null)
                    .setContinueMethod(Sequence.ContinueMethod.Overlay)
                    .build();

        }


        ChainTourGuide.init(getActivity()).playInSequence(sequence);
    }

    @Override
    public void onResume() {
        super.onResume();
        Logger.d(TAG, "onResume");
        /*SharedPreferences pref = getActivity().getSharedPreferences(Helper.APP_SHARED_PREF, 0);
        if(pref.getBoolean("show_help",true)){
            helpSequence();
            SharedPreferences.Editor editor = pref.edit();
            editor.putBoolean("show_help", false);
            editor.apply();
        }*/
    }
    private void showHelp(final String MENU){
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(new ContextThemeWrapper(getActivity(), R.style.myDialog));
        final SharedPreferences pref = getActivity().getSharedPreferences(Helper.APP_SHARED_PREF, Context.MODE_PRIVATE);
        final SharedPreferences.Editor editor = pref.edit();
        boolean showDialog=false;
        switch (MENU){
            case "UPLOAD":

                if(pref.getBoolean("show_upload_help", true)){
                    editor.putBoolean("show_upload_help", false);
                    editor.apply();
                alertDialog.setTitle(getString(R.string.helpUploadTitle));
                alertDialog.setMessage(getString(R.string.helpUploadDescription));
                    showDialog=true;
                }else{
                    iconUploadClicked();
                }
                break;
            case "TRANSLATE":

                if(pref.getBoolean("show_translate_help", true)) {
                    editor.putBoolean("show_translate_help", false);
                    editor.apply();

                    alertDialog.setTitle(getString(R.string.helpTranslateTitle));
                    alertDialog.setMessage(getString(R.string.helpTranslateDescription));
                    showDialog=true;
                }else{
                    iconTranslateClicked();
                }
                break;
            case "MAP":
                if(pref.getBoolean("show_map_help", true)) {
                    editor.putBoolean("show_map_help", false);
                    editor.apply();
                    alertDialog.setTitle(getString(R.string.helpMapTitle));
                    alertDialog.setMessage(getString(R.string.helpMapDescription));
                    showDialog=true;
                }else{
                    iconMapClicked();
                }
                break;
            case "SHARE":
                if(pref.getBoolean("show_share_help", true)) {
                    editor.putBoolean("show_share_help", false);
                    editor.apply();
                    alertDialog.setTitle(getString(R.string.helpShareTitle));
                    alertDialog.setMessage(getString(R.string.helpShareDescription));
                    showDialog=true;
                }else{
                    iconShareClicked();
                }
                break;
        }
        alertDialog.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,int which) {
                switch (MENU){
                    case "UPLOAD":
                        iconUploadClicked();
                        break;
                    case "TRANSLATE":
                        iconTranslateClicked();
                        break;
                    case "MAP":
                        iconMapClicked();
                        break;
                    case "SHARE":
                        iconShareClicked();
                        break;
                }
            }
        });
        /*alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });*/
        if(showDialog)
            alertDialog.show();
    }
}