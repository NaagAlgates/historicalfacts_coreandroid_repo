package com.myhistory.android.ta.bean;

/**
 * Created by Nagaraj.A on 9/7/2015.
 * Last Modified by : Nagaraj.A
 * Package Name : com.myhistory.android.ta.bean
 * Project Name : TamilHistoricalFacts
 */
public class StoryStatus {
    private String downloadStatus;
    private String socialStatus;

    public String getDownloadStatus() {
        return downloadStatus;
    }

    public void setDownloadStatus(String downloadStatus) {
        this.downloadStatus = downloadStatus;
    }

    public String getSocialStatus() {
        return socialStatus;
    }

    public void setSocialStatus(String socialStatus) {
        this.socialStatus = socialStatus;
    }
}
