package com.myhistory.android.ta;


import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import com.google.android.gcm.GCMBaseIntentService;
import com.myhistory.android.ta.activity.MainActivity;
import com.myhistory.android.ta.activity.SplashActivity;
import com.myhistory.android.ta.utility.GeneralHelper;
import com.myhistory.android.ta.utility.Helper;

public class GCMIntentService extends GCMBaseIntentService {

	public static String deviceRegID = null;

	// Context context;

	@SuppressWarnings("hiding")
	private static final String TAG = "GCMIntentService";

	public GCMIntentService() {
		super(Helper.DEFAULT_GCM_SENDER_ID);
	}

	@Override
	protected void onRegistered(Context context, String registrationId) {
		Log.i(TAG, "Device id from onRegister method: regId = "+ registrationId);
		deviceRegID = registrationId;
		SharedPreferences pref = context.getSharedPreferences(
		Helper.APP_SHARED_PREF, MODE_PRIVATE);
		Editor editor = pref.edit();
		editor.putString(Helper.GCM_REG_ID, registrationId);
		editor.apply();


	}

	@Override
	protected void onUnregistered(Context context, String registrationId) {
		Log.i(TAG, "Device unregistered");
	}

	@Override
	protected void onMessage(Context context, Intent intent) {
		try {
			// Log.i(TAG, "Received message. Extras: " + intent.getExtras());
			//String message = getString(R.string.gcm_message);
			// this.context = context;

			Bundle bundle = intent.getExtras();
			if (bundle != null) {
				bundle.getString("message");
				//SharedPreferences pref = context.getSharedPreferences("Gcm",MODE_PRIVATE);
				/*if (bundle.getString("message").contains("Task has been Updated")
						|| bundle.getString("message").contains("Task has been created")||
						bundle.getString("message").contains("Task has been Deleted"))  {
					Log.v(TAG,"Action has occurred");*/
					// context.stopService(new Intent(context, GcmListenService.class));

					Log.v(TAG, bundle.getString("message"));
					//sendNotification(context, bundle.getString("message"));
					//updateMyActivity(context, bundle.getString("message"));
					//DisplayEventAction(context, bundle.getString("message"));

				/*}else{
					Log.v(TAG, bundle.getString("message"));
					//sendNotification(context, bundle.getString("message"));
					//DisplayEventAction(context, bundle.getString("message"));
				}*/
				SharedPreferences pref = getSharedPreferences(Helper.APP_SHARED_PREF, MODE_PRIVATE);
				SharedPreferences.Editor editor = pref.edit();
				editor.putBoolean(Helper.PREF_HEAVY_LOAD, true);
				editor.apply();
				GeneralHelper.setBadge(this, 1);
				sendNotification(context, bundle.getString("message"));
				/* if(bundle.getString("message").contains("Nice!") ||
						bundle.getString("message").contains("has completed the task")){
					Log.v(TAG,"Completed task Parent");
				}

				Log.i(TAG,
						"MyCal Notification: " + bundle.getString("message"));

				// generateNotification(context, ""+bundle.getString("message"));
				//if (bundle.getString("message").contains("FamilyZone:")) {
				sendNotification(context, bundle.getString("message"));
				//}*/
			}
		}catch(Exception e){
			e.printStackTrace();
		}

	}

	@Override
	protected void onDeletedMessages(Context context, int total){
			Log.i(TAG, "Received deleted messages notification");
	}

	@Override
	public void onError(Context context, String errorId) {
		Log.i(TAG, "Received error: " + errorId);
	}

	@Override
	protected boolean onRecoverableError(Context context, String errorId) {
		// log message
		Log.i(TAG, "Received recoverable error: " + errorId);
		return super.onRecoverableError(context, errorId);
	}

	// Put the GCM message into a notification and post it.
	private void sendNotification(Context context, String msg) {
		try {
			NotificationManager mNotificationManager = (NotificationManager) context
					.getSystemService(Context.NOTIFICATION_SERVICE);

			Intent intent = new Intent(context, SplashActivity.class);
			intent.putExtra("DISPLAY_MESSAGE", msg);
			Log.v("DISPLAY_MESSAGE",msg);
			PendingIntent contentIntent = PendingIntent.getActivity(context, 0, intent,PendingIntent.FLAG_UPDATE_CURRENT);
					//new Intent(context, DisplayActionEvent.class), 0);

					//contentIntent.getActivity(context, 0,intent, 0);

					NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
							context).setSmallIcon(R.mipmap.ic_launcher)
							.setContentTitle(getString(R.string.app_name))
							.setStyle(new NotificationCompat.BigTextStyle().bigText(msg))
							.setAutoCancel(true).setContentText(msg);

			mBuilder.setContentIntent(contentIntent);
			mNotificationManager.notify(1, mBuilder.build());
		}catch (Exception e){
			e.printStackTrace();
		}
	}
	/*static void updateMyActivity(Context context, String message) {
		try {
			System.out.println("updateMyActivity " + message);
			Intent intent = new Intent(context.getString(R.string.BRMessage));
			intent.putExtra("message", message);
			context.sendBroadcast(intent);
		}catch (Exception e){
			e.printStackTrace();
		}
	}*/
	/*static void DisplayEventAction(Context context, String message) {
		try {
			System.out.println("updateMyActivity " + message);
			Intent intent = new Intent(context.getString(R.string.BRAction));
			intent.putExtra("message", message);
			context.sendBroadcast(intent);
		}catch (Exception e){
			e.printStackTrace();
		}
	}*/

}
