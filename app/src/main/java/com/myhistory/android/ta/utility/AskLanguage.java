package com.myhistory.android.ta.utility;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.myhistory.android.ta.App;
import com.myhistory.android.ta.R;
import com.myhistory.android.ta.events.LanguageChange;
import com.myhistory.android.ta.events.SetLanguage;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by NA112077 on 10/27/2016.
 * Last Edited by NA112077 on 10/27/2016
 * Project Name : historicalfacts_coreandroid_repo
 * Package Name : com.myhistory.android.ta.utility
 */

public class AskLanguage extends Dialog implements
        android.view.View.OnClickListener {
    private RadioGroup languageGroup;
    public Activity c;
    //public Dialog d;
    public boolean isSplash;

    public AskLanguage(Activity a,boolean isSplash) {
        super(a);
        // TODO Auto-generated constructor stub
        this.c = a;
        this.isSplash = isSplash;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL, WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH, WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH);
        String sLang=App.get().getLang();
        if(sLang!=null) {
            App.get().setLocale(sLang);
        }
        setContentView(R.layout.dialog_language);
        //d.setCancelable(false);
        Button yes = (Button) findViewById(R.id.btn_yes);
        Button cancel = (Button) findViewById(R.id.btn_cancel);
        languageGroup = (RadioGroup) findViewById(R.id.languageGroup);
        RadioButton langTamil = (RadioButton) findViewById(R.id.langTamil);
        RadioButton langEnglish = (RadioButton) findViewById(R.id.langEnglish);
        RadioButton langHindi = (RadioButton) findViewById(R.id.langHindi);
        RadioButton langMalayalam = (RadioButton) findViewById(R.id.langMalayalam);
        RadioButton langTelugu = (RadioButton) findViewById(R.id.langTelugu);
        String selLanguage = App.get().getLang();
        if(selLanguage!=null && selLanguage.length()>0) {
            //String selLanguage = App.get().getSelectedLanguageCode(sLanguage);
            switch (selLanguage) {
                case "en":
                    langEnglish.setChecked(true);
                    break;
                case "ta":
                    langTamil.setChecked(true);
                    break;
                case "hi":
                    langHindi.setChecked(true);
                    break;
                case "ml":
                    langMalayalam.setChecked(true);
                    break;
                case "te":
                    langTelugu.setChecked(true);
                    break;
            }
        }

        yes.setOnClickListener(this);
        cancel.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_yes:
                int selectedLanguageID = languageGroup.getCheckedRadioButtonId();
                RadioButton selectedLanguage = (RadioButton) findViewById(selectedLanguageID);
                String sLanguage = selectedLanguage.getText().toString();
                String sExistingLanguage=App.get().getLang();
                if(sExistingLanguage==null || !sExistingLanguage.equals(App.get().getSelectedLanguageCode(sLanguage))) {
                    EventBus.getDefault().post(new SetLanguage(sLanguage, isSplash));
                }
                dismiss();
                break;
            case R.id.btn_cancel:
                EventBus.getDefault().post(new LanguageChange(false,isSplash));
                dismiss();
                break;
            default:
                EventBus.getDefault().post(new LanguageChange(false,isSplash));
                break;
        }
        dismiss();
    }

    @Override
    public boolean onTouchEvent(@NonNull MotionEvent event) {
        // If we've received a touch notification that the user has touched
        // outside the app, finish the activity.
        if (MotionEvent.ACTION_OUTSIDE == event.getAction()) {
            //finish();
            return true;
        }

        // Delegate everything else to Activity.
        return super.onTouchEvent(event);
    }

}