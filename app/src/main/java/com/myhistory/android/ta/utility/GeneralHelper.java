package com.myhistory.android.ta.utility;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.widget.ImageView;

import com.myhistory.android.ta.R;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Random;

import static com.myhistory.android.ta.utility.Helper.PERMISSIONS_STORAGE;
import static com.myhistory.android.ta.utility.Helper.REQUEST_EXTERNAL_STORAGE;

/**
 * Created by Nagaraj.A on 8/18/2015.
 * Last Modified by : Nagaraj.A
 * Package Name : com.myhistory.android.ta.utility
 * Project Name : TamilHistoricalFacts
 */
public class GeneralHelper {
    //static String uniqueID = null;
    static String TAG = "GeneralHelper";

    public static String getDeviceName() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return capitalize(model);
        } else {
            return "Device Manufacturer : "+capitalize(manufacturer) + " " + model;
        }
    }

    public static int getRandomNumber(int maxNumber){
        Random rand = new Random();
        return rand.nextInt(maxNumber);
    }
    private static String capitalize(String s) {
        if (s == null || s.length() == 0) {
            return "";
        }
        char first = s.charAt(0);
        if (Character.isUpperCase(first)) {
            return s;
        } else {
            return Character.toUpperCase(first) + s.substring(1);
        }
    }

    public static Uri getLocalBitmapUri(ImageView imageView) {
        // Extract Bitmap from ImageView drawable
        Drawable drawable = imageView.getDrawable();
        Bitmap bmp;// = null;
        if (drawable instanceof BitmapDrawable){
            bmp = ((BitmapDrawable) imageView.getDrawable()).getBitmap();
        } else {
            return null;
        }
        // Store image to default external storage directory
        Uri bmpUri = null;
        try {
            File file =  new File(Environment.getExternalStoragePublicDirectory(
                    Environment.DIRECTORY_DOWNLOADS), "share_image_" + System.currentTimeMillis() + ".jpg");
            if(!file.getParentFile().mkdirs()) {
                Log.v(TAG,"Folder does not exists");
            }
            FileOutputStream out = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.PNG, 90, out);
            out.close();
            bmpUri = Uri.fromFile(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bmpUri;
    }

    public static String getAndroidVersion() {
        String release = Build.VERSION.RELEASE;
        int sdkVersion = Build.VERSION.SDK_INT;
        return "Android Version: " + sdkVersion + " (" + release +")";
    }

    /*public synchronized static String getUniqueID(Context context) {
        if (uniqueID == null)
        {
            SharedPreferences sharedPrefs = context.getSharedPreferences(IConstants_Android.PREF_UNIQUE_ID, Context.MODE_PRIVATE);
            uniqueID = sharedPrefs.getString(IConstants_Android.PREF_UNIQUE_ID, null);
            if (uniqueID == null)
            {
                uniqueID = UUID.randomUUID().toString();
                SharedPreferences.Editor editor = sharedPrefs.edit();
                editor.putString(IConstants_Android.PREF_UNIQUE_ID, uniqueID);
                editor.apply();
            }
        }
        return uniqueID;
    }*/
    public static String getApplicationName(Context context) {
        int stringId = context.getApplicationInfo().labelRes;
        return context.getString(stringId);
    }
    public static String removeLastCharacter(String str,char removeChar) {
        if (str.length() > 0 && str.charAt(str.length()-1)== removeChar) {
            str = str.substring(0, str.length()-1);
        }
        return str;
    }
    public static String httpConnection(String jsonURL){
        String responseJSON=null;
        try {
            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(jsonURL)
                    .build();
            Response response = client.newCall(request).execute();
            responseJSON = response.body().string();
        }catch (Exception e){
            e.printStackTrace();
        }
        return responseJSON;
    }
    public static String changeDateFormat(String oldFormat){
        String newFormat;
        String oldFormatDate[] = oldFormat.split("-");
        newFormat=oldFormatDate[2]+"-"+oldFormatDate[1]+"-"+oldFormatDate[0];
        Log.v(TAG,newFormat);
        return newFormat;
    }
    public static long getDateDifference(String dbDate){
        //int dateDiff=0;
        String currentDate;
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        Calendar c = Calendar.getInstance();
        System.out.println("Current time => " + c.getTime());
        currentDate = formatter.format(c.getTime());
        try {
            Date fromDbDate = formatter.parse(dbDate);
            Date fromCurrDate = formatter.parse(currentDate);
            //long diffInDays = fromCurrDate.getTime() - fromDbDate.getTime();
            /*long diffInDays = ( (fromCurrDate.getTime() - fromDbDate.getTime())
                    / (1000 * 60 * 60 * 24) );*/
            return ( (fromCurrDate.getTime() - fromDbDate.getTime())
                    / (1000 * 60 * 60 * 24) );//timeUnit.convert(diffInDays, TimeUnit.DAYS);
        }catch (Exception e){
            e.printStackTrace();
            return 0;
        }
        //return  dateDiff;
    }
    public static boolean checkInternetConnection(Context context) {
        try {
            Log.v(TAG,"checkInternetConnection");
            /*ConnectivityManager localConnectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo[] arrayOfNetworkInfo = null;
            if (localConnectivityManager != null) {
                arrayOfNetworkInfo = localConnectivityManager.getAllNetworkInfo();
                if (arrayOfNetworkInfo == null){
                    Log.v(TAG,"arrayOfNetworkInfo is null");
                }
            }
            for (int i = 0; ; i++) {
                if (arrayOfNetworkInfo != null && i >= arrayOfNetworkInfo.length) {
                    Log.v(TAG,"UnAvailable");
                    return false;
                }
                if (arrayOfNetworkInfo != null && arrayOfNetworkInfo[i].getState() == NetworkInfo.State.CONNECTED) {
                    Log.v(TAG,"Available");
                    return true;
                }
            }*/
            Runtime runtime = Runtime.getRuntime();
            try {

                Process ipProcess = runtime.exec("/system/bin/ping -c 1 8.8.8.8");
                int     exitValue = ipProcess.waitFor();
                return (exitValue == 0);

            } catch (IOException | InterruptedException e)          { e.printStackTrace(); }

            return false;
        }catch (Exception e){
            e.printStackTrace();
            Log.v(TAG, "checkInternetConnection_Exception");
            return false;
        }
    }
    public static void setBadge(Context context, int count) {
        String launcherClassName = getLauncherClassName(context);
        Logger.v(TAG,launcherClassName);
        if (launcherClassName == null) {
            return;
        }
        Intent intent = new Intent("android.intent.action.BADGE_COUNT_UPDATE");
        intent.putExtra("badge_count", count);
        Logger.v(TAG,""+count);
        intent.putExtra("badge_count_package_name", context.getPackageName());
        intent.putExtra("badge_count_class_name", launcherClassName);
        context.sendBroadcast(intent);
    }

    private static String getLauncherClassName(Context context) {

        PackageManager pm = context.getPackageManager();

        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_LAUNCHER);

        List<ResolveInfo> resolveInfos = pm.queryIntentActivities(intent, 0);
        for (ResolveInfo resolveInfo : resolveInfos) {
            String pkgName = resolveInfo.activityInfo.applicationInfo.packageName;
            if (pkgName.equalsIgnoreCase(context.getPackageName())) {

                return  resolveInfo.activityInfo.name;
            }
        }
        return null;
    }

    public static void verifyStoragePermissions(Activity activity) {
        // Check if we have write permission
        int permission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
        }
    }



}
