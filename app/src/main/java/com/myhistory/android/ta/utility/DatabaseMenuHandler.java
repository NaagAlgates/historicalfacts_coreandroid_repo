package com.myhistory.android.ta.utility;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.myhistory.android.ta.App;
import com.myhistory.android.ta.bean.DisplayStoryDetails;
import com.myhistory.android.ta.bean.StoryDetails;
import com.myhistory.android.ta.bean.StoryStatus;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nagaraj.A on 8/27/2015.
 * Last Modified by : Nagaraj.A
 * Package Name : com.myhistory.android.ta.utility
 * Project Name : TamilHistoricalFacts
 */
public class DatabaseMenuHandler extends SQLiteOpenHelper {
    private static String DATABASE_NAME = Helper.DATABASE_NAME;
    private static int DATABASE_VERSION = Helper.DATABASE_VERSION;
    private static String TAG = "DatabaseMenuHandler";
    private static String TBL_STORY_DETAILS_TABLE_NAME = "TBL_STORY_DETAILS";

    public DatabaseMenuHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        try {
            /*String TBL_STORY_DETAILS = "CREATE TABLE "+TBL_STORY_DETAILS_TABLE_NAME+" (story_id INTEGER UNIQUE NOT NULL, eng_title TEXT, " +
                    "tam_title TEXT, eng_brief TEXT, tam_brief TEXT, display_pic TEXT, " +
                    "inserted_datetime TEXT, place_name TEXT, geo_location TEXT, story_status INTEGER DEFAULT (1)," +
                    " sno INTEGER PRIMARY KEY AUTOINCREMENT, read_status INTEGER DEFAULT (0), social_status INTEGER DEFAULT (0)," +
                    " download_status INTEGER DEFAULT (0))";
            db.execSQL(TBL_STORY_DETAILS);*/
            createTables(db);
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    public void deleteTables(){
        SQLiteDatabase db = this.getWritableDatabase();
        //db.execSQL("DELETE FROM "+TBL_STORY_DETAILS_TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS "+TBL_STORY_DETAILS_TABLE_NAME);
        createTables(db);
        db.close();
    }
    private void createTables(SQLiteDatabase sqLiteDatabase){
        String TBL_STORY_DETAILS = "CREATE TABLE "+TBL_STORY_DETAILS_TABLE_NAME+" (story_id INTEGER UNIQUE NOT NULL, eng_title TEXT, " +
                "tam_title TEXT, eng_brief TEXT, tam_brief TEXT, display_pic TEXT, " +
                "inserted_datetime TEXT, place_name TEXT, geo_location TEXT, story_status INTEGER DEFAULT (1)," +
                " sno INTEGER PRIMARY KEY AUTOINCREMENT, read_status INTEGER DEFAULT (0), social_status INTEGER DEFAULT (0)," +
                " download_status INTEGER DEFAULT (0))";
        sqLiteDatabase.execSQL(TBL_STORY_DETAILS);
        //sqLiteDatabase.close();
    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //Log.v(TAG, "onUpgrade");
        try {
            db.execSQL("DROP TABLE IF EXISTS TBL_STORY_DETAILS");
            onCreate(db);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public boolean insertStoryDetails(String storyID, String engTitle, String tamilTitle, String endBrief,
                                       String tamilBrief,String displayPic,String insertedDateTime,
                                      String locationName,String geoLocation) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            //try {
            ContentValues values = new ContentValues();
            values.put("story_id", storyID);
            values.put("eng_title", engTitle);
            values.put("tam_title", tamilTitle);
            values.put("eng_brief", endBrief);
            values.put("tam_brief", tamilBrief);
            values.put("display_pic", displayPic);
            values.put("inserted_datetime", insertedDateTime);
            values.put("place_name", locationName);
            values.put("geo_location", geoLocation);
            db.insert("TBL_STORY_DETAILS", null, values);
        }catch (Exception e){
            e.printStackTrace();
        }
            return true;
        /*} catch (Exception e) {
            return false;
        }*/
    }
    public List<DisplayStoryDetails> storyDetailsList(int min,int max,int sortType){
        String sQuery=""; /*= "SELECT story_id,tam_title,display_pic,inserted_datetime,geo_location,read_status,social_status,tam_brief,sno " +
                "FROM TBL_STORY_DETAILS  WHERE sno >= "+min+" AND sno <= "+max+" ORDER BY story_id DESC";*/
        switch (sortType){
            case 1:
                //விரும்பியவை
                sQuery=" SELECT story_id,tam_title,display_pic,inserted_datetime,geo_location,read_status,social_status,tam_brief," +
                        " sno,sno_cnt FROM(SELECT *, (SELECT COUNT(*) FROM TBL_STORY_DETAILS b WHERE b.sno >= a.sno AND " +
                        " b.social_status = 1 ORDER BY b.sno DESC ) AS sno_cnt  FROM TBL_STORY_DETAILS a WHERE a.social_status = 1 " +
                        " ORDER BY a.sno DESC ) c WHERE c.sno_cnt >="+min+" AND c.sno_cnt <= "+max;
                break;
            case 2:
                //நட்சத்திரமிட்டவை
                sQuery=" SELECT story_id,tam_title,display_pic,inserted_datetime,geo_location,read_status,social_status,tam_brief," +
                        " sno,sno_cnt FROM(SELECT *, (SELECT COUNT(*) FROM TBL_STORY_DETAILS b WHERE b.sno >= a.sno AND " +
                        " b.download_status = 1 ORDER BY b.sno DESC ) AS sno_cnt  FROM TBL_STORY_DETAILS a WHERE a.download_status = 1 " +
                        " ORDER BY a.sno DESC ) c WHERE c.sno_cnt >="+min+" AND c.sno_cnt <= "+max;
                break;
            case 3:
                //வழிசெலுத்தல் உள்ளவை
                sQuery=" SELECT story_id,tam_title,display_pic,inserted_datetime,geo_location,read_status,social_status,tam_brief," +
                        " sno,sno_cnt FROM(SELECT *, (SELECT COUNT(*) FROM TBL_STORY_DETAILS b WHERE b.sno >= a.sno AND " +
                        " b.geo_location <> 0 ORDER BY b.sno DESC ) AS sno_cnt  FROM TBL_STORY_DETAILS a WHERE a.geo_location <> 0 " +
                        " ORDER BY a.sno DESC ) c WHERE c.sno_cnt >="+min+" AND c.sno_cnt <= "+max;
                break;
            default:
                //அனைத்தும்
                String sLanguage = App.get().getLang();
                switch(sLanguage){
                    case "ta":
                        sQuery=" SELECT story_id,tam_title,display_pic,inserted_datetime,geo_location,read_status,social_status,tam_brief," +
                                " sno,sno_cnt FROM(SELECT *, (SELECT COUNT(*) FROM TBL_STORY_DETAILS b WHERE b.sno >= a.sno  " +
                                " ORDER BY b.sno DESC ) AS sno_cnt  FROM TBL_STORY_DETAILS a ORDER BY a.sno DESC ) c " +
                                " WHERE c.sno_cnt >= "+ min +" AND c.sno_cnt <= "+max+" ORDER BY sno DESC";
                        break;
                    case "en":
                    default:
                        sQuery=" SELECT story_id,eng_title,display_pic,inserted_datetime,geo_location,read_status,social_status,eng_brief," +
                                " sno,sno_cnt FROM(SELECT *, (SELECT COUNT(*) FROM TBL_STORY_DETAILS b WHERE b.sno >= a.sno  " +
                                " ORDER BY b.sno DESC ) AS sno_cnt  FROM TBL_STORY_DETAILS a ORDER BY a.sno DESC ) c " +
                                " WHERE c.sno_cnt >= "+ min +" AND c.sno_cnt <= "+max+" ORDER BY sno DESC";
                        break;
                }
                break;
            /*case 4:
                sQuery="SELECT story_id,tam_title,display_pic,inserted_datetime,geo_location,read_status,social_status,tam_brief,sno " +
                        "FROM TBL_STORY_DETAILS  WHERE sno >= "+min+" AND sno <= "+max+" ORDER BY download_status DESC";
                break;
            case 5:
                sQuery="SELECT story_id,tam_title,display_pic,inserted_datetime,geo_location,read_status,social_status,tam_brief,sno " +
                        "FROM TBL_STORY_DETAILS  WHERE sno >= "+min+" AND sno <= "+max+" ORDER BY inserted_datetime";
                break;
            case 6:
                sQuery="SELECT story_id,tam_title,display_pic,inserted_datetime,geo_location,read_status,social_status,tam_brief,sno " +
                        "FROM TBL_STORY_DETAILS  WHERE sno >= "+min+" AND sno <= "+max+" ORDER BY inserted_datetime DESC";
                break;
            case 7:
                sQuery="SELECT story_id,tam_title,display_pic,inserted_datetime,geo_location,read_status,social_status,tam_brief,sno " +
                        "FROM TBL_STORY_DETAILS  WHERE sno >= "+min+" AND sno <= "+max+" ORDER BY geo_location DESC ";
                break;
            case 8:
                sQuery="SELECT story_id,tam_title,display_pic,inserted_datetime,geo_location,read_status,social_status,tam_brief,sno " +
                        "FROM TBL_STORY_DETAILS  WHERE sno >= "+min+" AND sno <= "+max+" ORDER BY geo_location ";
                break;
            default:
                sQuery = "SELECT story_id,tam_title,display_pic,inserted_datetime,geo_location,read_status,social_status,tam_brief,sno " +
                        "FROM TBL_STORY_DETAILS  WHERE sno >= "+min+" AND sno <= "+max+" ORDER BY story_id DESC";
                break;*/
        }
        Log.v(TAG, sQuery);
        if(sQuery.length()>2) {
            SQLiteDatabase db = this.getReadableDatabase();
            Cursor c = db.rawQuery(sQuery, null);
            List<DisplayStoryDetails> storyDetailsArrayList = new ArrayList<>();
            if (c.moveToFirst()) {
                do {
                    DisplayStoryDetails storyDetails = new DisplayStoryDetails();
                    storyDetails.setStory_id(c.getString(0));
                    storyDetails.setTam_title(c.getString(1));
                    storyDetails.setDisplay_pic(c.getString(2));
                    storyDetails.setInserted_datetime(c.getString(3));
                    storyDetails.setGeo_location(c.getString(4));
                    storyDetails.setRead_status(c.getString(5));
                    storyDetails.setSocial_status(c.getString(6));
                    storyDetails.setTam_content(c.getString(7));
                    storyDetails.setSno(c.getString(8));
                    storyDetailsArrayList.add(storyDetails);
                } while (c.moveToNext());
            }
            c.close();
            db.close();
            return storyDetailsArrayList;
        }else{
            return  null;
        }
    }
    public  List<StoryStatus> getStoryStatusList(String sno){
        String sQuery = "SELECT download_status,social_status FROM TBL_STORY_DETAILS WHERE story_id = "+sno;
        Log.v(TAG, sQuery);
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(sQuery, null);
        List<StoryStatus> storyStatus = new ArrayList<>();
        if (c.moveToFirst()) {
            do {
                StoryStatus status = new StoryStatus();
                status.setDownloadStatus(c.getString(0));
                Log.v(TAG, c.getString(0));
                status.setSocialStatus(c.getString(1));
                Log.v(TAG, c.getString(1));
                storyStatus.add(status);
            }while (c.moveToNext());
        }
        c.close();
        db.close();
        return  storyStatus;
    }
    public int getCountFromTable(String sQuery) {
        int count = 0;
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            Cursor mCount = db.rawQuery(sQuery, null);
            mCount.moveToFirst();
            count = mCount.getInt(0);
            mCount.close();
            db.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return count;
    }
    public String getLastStoryID(String sQuery) {
        String count = "0";
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            Cursor mCount = db.rawQuery(sQuery, null);
            if(mCount!=null&&mCount.getCount()>0) {
                mCount.moveToFirst();
                count = "" + mCount.getInt(0);
                mCount.close();
            }
            db.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return count;
    }
    public Boolean updateAsRead(String storySno){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("read_status", "1");//Story Read
        db.update("TBL_STORY_DETAILS", values, "story_id" + " = ?",
                new String[] { String.valueOf(storySno) });
        db.close();
        return true;
    }
    public Boolean updateSocialStatus(String storySno,String storyStatus){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("social_status", storyStatus);//Story Read
        db.update("TBL_STORY_DETAILS", values, "story_id" + " = ?",
                new String[]{String.valueOf(storySno) });
        db.close();
        return true;
    }
    public Boolean updateDownloadStatus(String storySno,String storyStatus){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("download_status", storyStatus);//Story Read
        db.update("TBL_STORY_DETAILS", values, "story_id" + " = ?",
                new String[]{String.valueOf(storySno) });
        db.close();
        return true;
    }
}
