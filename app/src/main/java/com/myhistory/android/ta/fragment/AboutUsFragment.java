package com.myhistory.android.ta.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.myhistory.android.ta.R;
import com.myhistory.android.ta.adapter.AboutUsListViewAdapter;

import java.util.ArrayList;

/**
 * Created by Nagaraj.A on 8/17/2015.
 * Last Modified by : Nagaraj.A
 * Package Name : com.myhistory.android.ta.fragment
 * Project Name : Tamil Historical Facts
 */
public class AboutUsFragment extends Fragment {
    View rootView;
    AdView mAdView;
    private static String TAG="AboutUsFragment";
    AboutUsListViewAdapter mAdapter;
    ListView aboutUSListView;
    private ArrayList<Integer> listDisplayImage=new ArrayList<>();
    private ArrayList<String> listPersonName=new ArrayList<>();
    private ArrayList<String> listShortIntro=new ArrayList<>();
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_aboutus,container,false);
        return rootView;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        Log.v(TAG, "onSaveInstanceState");
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onStart() {
        Log.v(TAG, "onSaveInstanceState");
        super.onStart();

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        Log.v(TAG, "onActivityCreated");
        super.onActivityCreated(savedInstanceState);
        try
        {
            Initialize();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public void onPause() {
        Log.v(TAG, "onPause");
        super.onPause();
    }

    @Override
    public void onResume() {
        Log.v(TAG, "onResume");
        super.onResume();
    }

    @Override
    public void onStop() {
        Log.v(TAG, "onStop");
        super.onStop();
    }

    @Override
    public void onDestroyView() {
        Log.v(TAG, "onDestroyView");
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        Log.v(TAG, "onDestroy");
        super.onDestroy();
    }

    @Override
    public void onDetach() {
        Log.v(TAG, "onDetach");
        super.onDetach();
    }

    private void Initialize() {
        putValues();
        /*mAdView = (AdView)  getActivity().findViewById(R.id.ad_view_aboutus);
        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                .build();
        mAdView.loadAd(adRequest);*/
        aboutUSListView = (ListView) getActivity().findViewById(R.id.AboutUsList);
        mAdapter = new AboutUsListViewAdapter(getActivity(), listPersonName, listShortIntro,listDisplayImage);
        Log.v(TAG,"Adapter_over");
        aboutUSListView.setAdapter(mAdapter);
    }
    private void putValues()
    {
        listPersonName.add(getString( R.string.name1));
        listPersonName.add(getString( R.string.name2));
        listPersonName.add(getString( R.string.name3));

        listShortIntro.add(getString( R.string.Cont1));
        listShortIntro.add(getString( R.string.Cont2));
        listShortIntro.add(getString( R.string.Cont2));

        listDisplayImage.add(R.drawable.sasi);
        listDisplayImage.add(R.drawable.venk);
        listDisplayImage.add(R.drawable.naag);
    }

}