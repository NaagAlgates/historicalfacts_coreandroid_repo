package com.myhistory.android.ta.activity;

import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.app.NavUtils;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.myhistory.android.ta.App;
import com.myhistory.android.ta.R;
import com.myhistory.android.ta.essentials.ZoomOutPageTransformer;
import com.myhistory.android.ta.fragment.ScreenSlidePageFragment;
import com.myhistory.android.ta.utility.Logger;

/**
 * Created by Nagaraj.A on 9/4/2015.
 * Last Modified by : Nagaraj.A
 * Package Name : com.myhistory.android.ta.activity
 * Project Name : TamilHistoricalFacts
 */
public class ScreenSlideActivity extends AppCompatActivity {
    /**
     * The number of pages (wizard steps) to show in this demo.
     */
    private static int NUM_PAGES;
    int selectedSno;

    /**
     * The pager widget, which handles animation and allows swiping horizontally to access previous
     * and next wizard steps.
     */
    private ViewPager mPager;
    AdView mAdView;
    private static String TAG = ScreenSlideActivity.class.getSimpleName();


    /**
     * The pager adapter, which provides the pages to the view pager widget.
     */
    PagerAdapter mPagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        App.get().setLocale(App.get().getLang());
        setContentView(R.layout.activity_screen_slide);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            //window.setStatusBarColor(getResources().getColor(R.color.app_actionbar_background));
            window.setStatusBarColor(ContextCompat.getColor(getApplicationContext(), R.color.app_actionbar_background));
        }

        // Instantiate a ViewPager and a PagerAdapter.
        Intent intent = getIntent();
        selectedSno = Integer.parseInt(intent.getStringExtra("CLICKED_SNO"));
        Logger.v(TAG,"SELECTED NO : "+selectedSno);
        NUM_PAGES = intent.getIntExtra("TOTAL_STORY_COUNT",0);//Integer.parseInt(intent.getStringExtra("CLICKED_SNO"));
        mAdView = (AdView)  findViewById(R.id.ad_view_story);
        AdRequest adRequest = new AdRequest.Builder()
                .build();
        mAdView.loadAd(adRequest);
        mPager = (ViewPager) findViewById(R.id.pager);
        mPager.setPageTransformer(true, new ZoomOutPageTransformer());
        mPagerAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager());
        mPager.setAdapter(mPagerAdapter);
        mPager.setCurrentItem(selectedSno-1);
        mPager.setOffscreenPageLimit(1);
        mPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                invalidateOptionsMenu();
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                //Toast.makeText(getApplicationContext(), "Ad is loaded!", Toast.LENGTH_SHORT).show();
                Logger.v(TAG,"Ad loaded");
                mAdView.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAdClosed() {
                //Toast.makeText(getApplicationContext(), "Ad is closed!", Toast.LENGTH_SHORT).show();
                Logger.v(TAG,"Ad closed");
                mAdView.setVisibility(View.GONE);
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                //Toast.makeText(getApplicationContext(), "Ad failed to load! error code: " + errorCode, Toast.LENGTH_SHORT).show();
                Logger.v(TAG,"Ad failed to load. Error code : "+errorCode);
                mAdView.setVisibility(View.GONE);
            }

            @Override
            public void onAdLeftApplication() {
                //Toast.makeText(getApplicationContext(), "Ad left application!", Toast.LENGTH_SHORT).show();
                Logger.v(TAG,"Ad left application");
                mAdView.setVisibility(View.GONE);
            }

            @Override
            public void onAdOpened() {
                //Toast.makeText(getApplicationContext(), "Ad is opened!", Toast.LENGTH_SHORT).show();
                Logger.v(TAG,"Ad opened");
            }
        });
    }

    @Override
    public void onBackPressed() {
        /*if (mPager.getCurrentItem() == 0) {
            // If the user is currently looking at the first step, allow the system to handle the
            // Back button. This calls finish() on this activity and pops the back stack.
            super.onBackPressed();
        } else {
            // Otherwise, select the previous step.
            mPager.setCurrentItem(mPager.getCurrentItem() - 1);
        }*/
        super.onBackPressed();
        finish();
    }

    public void loadPrevious(View view) {
        loadPrevious();
    }

    public void loadNext(View view) {
        loadNext();
    }

    private void loadPrevious(){
        mPager.setCurrentItem(mPager.getCurrentItem() - 1);
    }
    private void loadNext(){
        mPager.setCurrentItem(mPager.getCurrentItem() + 1);
    }

    /**
     * A simple pager adapter that represents 5 ScreenSlidePageFragment objects, in
     * sequence.
     */
    public class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
        public ScreenSlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return ScreenSlidePageFragment.create(position);
        }

        @Override
        public int getCount() {
            return NUM_PAGES;
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.storymenu, menu);

        menu.findItem(R.id.action_previous).setEnabled(mPager.getCurrentItem() > 0);

        // Add either a "next" or "finish" button to the action bar, depending on which page
        // is currently selected.
        MenuItem item = menu.add(Menu.NONE, R.id.action_next, Menu.NONE,
                (mPager.getCurrentItem()-1 == mPagerAdapter.getCount())
                        ? R.string.completed
                        : R.string.next);
        item.setIcon(R.drawable.ic_action_next_item);
        item.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM | MenuItem.SHOW_AS_ACTION_WITH_TEXT);

        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // Navigate "up" the demo structure to the launchpad activity.
                // See http://developer.android.com/design/patterns/navigation.html for more.
                NavUtils.navigateUpTo(this, new Intent(this, MainActivity.class));
                return true;

            case R.id.action_previous:
                // Go to the previous step in the wizard. If there is no previous step,
                // setCurrentItem will do nothing.
                //mPager.setCurrentItem(mPager.getCurrentItem() - 1);
                loadPrevious();
                return true;

            case R.id.action_next:
                // Advance to the next step in the wizard. If there is no next step, setCurrentItem
                // will do nothing.
                //mPager.setCurrentItem(mPager.getCurrentItem() + 1);
                loadNext();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.d(TAG, "onStop");
        //EventBus.getDefault().unregister(App.get());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy");
        if (mAdView != null) {
            mAdView.destroy();
        }
    }

    public void donateUsClicked(View view) {
        Toast.makeText(App.get(),"சேவை விரைவில் இயக்குவிக்கப்படும்!",Toast.LENGTH_LONG).show();
    }
    @Override
    public void onPause() {
        super.onPause();
        Log.d(TAG, "onPause");
        if (mAdView != null) {
            mAdView.pause();
            Log.d(TAG, "onPause mAdView");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "onResume");
        if (mAdView != null) {
            mAdView.resume();
            Log.d(TAG, "onResume mAdView");
        }
    }

}
