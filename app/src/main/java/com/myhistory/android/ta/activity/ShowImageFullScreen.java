package com.myhistory.android.ta.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.PointF;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.myhistory.android.ta.App;
import com.myhistory.android.ta.R;
import com.myhistory.android.ta.utility.GeneralHelper;
import com.myhistory.android.ta.utility.Logger;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.File;
import java.io.FileOutputStream;

/**
 * Created by Nagaraj.A on 10/12/2015.
 * Last Modified by : Nagaraj.A
 * Package Name : com.myhistory.android.ta.activity
 * Project Name : TamilHistoricalFacts
 */
public class ShowImageFullScreen extends AppCompatActivity implements View.OnTouchListener {
    ImageView fullScreenImage;
    String selectedImageURL;
    private AdView adViewDownloadImage;
    private Handler handler;
    //private static final float MIN_ZOOM = 1f,MAX_ZOOM = 1f;
    // These matrices will be used to scale points of the image
    Matrix matrix = new Matrix();
    Matrix savedMatrix = new Matrix();

    // The 3 states (events) which the user is trying to perform
    static final int NONE = 0;
    static final int DRAG = 1;
    static final int ZOOM = 2;
    int mode = NONE;

    // these PointF objects are used to record the point(s) the user is touching
    PointF start = new PointF();
    PointF mid = new PointF();
    float oldDist = 1f;
    static String TAG="ShowImageFullScreen";
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        App.get().setLocale(App.get().getLang());
        setContentView(R.layout.activity_image_fullscreen);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            //window.setStatusBarColor(getResources().getColor(R.color.app_actionbar_background));
            window.setStatusBarColor(ContextCompat.getColor(getApplicationContext(), R.color.app_actionbar_background));
        }

        adViewDownloadImage = (AdView) findViewById(R.id.adViewDownloadImage);
        AdRequest adRequest = new AdRequest.Builder().build();
        adViewDownloadImage.loadAd(adRequest);
        adViewDownloadImage.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                //Toast.makeText(getApplicationContext(), "Ad is loaded!", Toast.LENGTH_SHORT).show();
                Logger.v(TAG,"Ad loaded");
                adViewDownloadImage.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAdClosed() {
                //Toast.makeText(getApplicationContext(), "Ad is closed!", Toast.LENGTH_SHORT).show();
                Logger.v(TAG,"Ad closed");
                adViewDownloadImage.setVisibility(View.GONE);
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                //Toast.makeText(getApplicationContext(), "Ad failed to load! error code: " + errorCode, Toast.LENGTH_SHORT).show();
                Logger.v(TAG,"Ad failed to load. Error code : "+errorCode);
                adViewDownloadImage.setVisibility(View.GONE);
            }

            @Override
            public void onAdLeftApplication() {
                //Toast.makeText(getApplicationContext(), "Ad left application!", Toast.LENGTH_SHORT).show();
                Logger.v(TAG,"Ad left application");
                adViewDownloadImage.setVisibility(View.GONE);
            }

            @Override
            public void onAdOpened() {
                //Toast.makeText(getApplicationContext(), "Ad is opened!", Toast.LENGTH_SHORT).show();
                Logger.v(TAG,"Ad opened");
            }
        });
        handler = new Handler();
        fullScreenImage = (ImageView) findViewById(R.id.fullScreenImage);
        fullScreenImage.setOnTouchListener(this);
        Intent intent = getIntent();
        selectedImageURL = intent.getStringExtra("IMAGE_URL");
        if(selectedImageURL!=null) {
            //fullScreenImage.setBackgroundResource(selectedProfile);
            /*Picasso.with(getApplicationContext())
                    .load(selectedImageURL)
                    .error(R.drawable.pageload_error)
                    .into(fullScreenImage);*/
            Log.v(TAG,selectedImageURL);
            Picasso.with(getApplicationContext())
                    .load(selectedImageURL)
                    .placeholder(R.drawable.placeholder)
                    .error(R.drawable.pageload_error)
                    .into(fullScreenImage);
        }
        else
            fullScreenImage.setBackgroundResource(R.mipmap.ic_launcher);
    }
    public void closeClicked(View v){
        finish();
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Log.d(TAG, "Back Pressed");
        finish();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.image_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id){
            case R.id.backMenu:
                finish();
                break;
        }


        return super.onOptionsItemSelected(item);
    }
    @Override
    public boolean onTouch(View v, MotionEvent event)
    {
        ImageView view = (ImageView) v;
        view.setScaleType(ImageView.ScaleType.MATRIX);
        float scale;

        dumpEvent(event);
        // Handle touch events here...

        switch (event.getAction() & MotionEvent.ACTION_MASK)
        {
            case MotionEvent.ACTION_DOWN:   // first finger down only
                savedMatrix.set(matrix);
                start.set(event.getX(), event.getY());
                Log.d(TAG, "mode=DRAG"); // write to LogCat
                mode = DRAG;
                break;

            case MotionEvent.ACTION_UP: // first finger lifted

            case MotionEvent.ACTION_POINTER_UP: // second finger lifted

                mode = NONE;
                Log.d(TAG, "mode=NONE");
                break;

            case MotionEvent.ACTION_POINTER_DOWN: // first and second finger down

                oldDist = spacing(event);
                Log.d(TAG, "oldDist=" + oldDist);
                if (oldDist > 5f) {
                    savedMatrix.set(matrix);
                    midPoint(mid, event);
                    mode = ZOOM;
                    Log.d(TAG, "mode=ZOOM");
                }
                break;

            case MotionEvent.ACTION_MOVE:

            if (mode == DRAG)
            {
                matrix.set(savedMatrix);
                matrix.postTranslate(event.getX() - start.x, event.getY() - start.y); // create the transformation in the matrix  of points
            }
            else if (mode == ZOOM)
            {
                // pinch zooming
                float newDist = spacing(event);
                Log.d(TAG, "newDist=" + newDist);
                if (newDist > 5f)
                {
                    matrix.set(savedMatrix);
                    scale = newDist / oldDist; // setting the scaling of the
                    // matrix...if scale > 1 means
                    // zoom in...if scale < 1 means
                    // zoom out
                    matrix.postScale(scale, scale, mid.x, mid.y);
                }
            }
            break;
        }
        /*limitZoom(matrix);
        limitDrag(matrix);*/
        view.setImageMatrix(matrix); // display the transformation on screen

        return true; // indicate event was handled
    }

    /*
     * --------------------------------------------------------------------------
     * Method: spacing Parameters: MotionEvent Returns: float Description:
     * checks the spacing between the two fingers on touch
     * ----------------------------------------------------
     */

    private float spacing(MotionEvent event)
    {
        float x = event.getX(0) - event.getX(1);
        float y = event.getY(0) - event.getY(1);
        return (float)Math.sqrt(x * x + y * y);
    }
    /*private void limitZoom(Matrix m) {

        float[] values = new float[9];
        m.getValues(values);
        float scaleX = values[Matrix.MSCALE_X];
        float scaleY = values[Matrix.MSCALE_Y];
        if(scaleX > MAX_ZOOM) {
            scaleX = MAX_ZOOM;
        } else if(scaleX < MIN_ZOOM) {
            scaleX = MIN_ZOOM;
        }

        if(scaleY > MAX_ZOOM) {
            scaleY = MAX_ZOOM;
        } else if(scaleY < MIN_ZOOM) {
            scaleY = MIN_ZOOM;
        }

        values[Matrix.MSCALE_X] = scaleX;
        values[Matrix.MSCALE_Y] = scaleY;
        m.setValues(values);
    }*/

    /*private void limitDrag(Matrix m) {
        float[] values = new float[9];
        m.getValues(values);
        float transX = values[Matrix.MTRANS_X];
        float transY = values[Matrix.MTRANS_Y];
        float scaleX = values[Matrix.MSCALE_X];
        float scaleY = values[Matrix.MSCALE_Y];

        //ImageView iv = (ImageView) findViewById(R.id.map);
        Rect bounds = fullScreenImage.getDrawable().getBounds();
        int viewWidth = getResources().getDisplayMetrics().widthPixels;
        int viewHeight = getResources().getDisplayMetrics().heightPixels;

        int width = bounds.right - bounds.left;
        int height = bounds.bottom - bounds.top;

        float minX = (-width + 20) * scaleX;
        float minY = (-height + 20) * scaleY;

        if (transX > (viewWidth - 20)) {
            transX = viewWidth - 20;
        } else if (transX < minX) {
            transX = minX;
        }

        if (transY > (viewHeight - 80)) {
            transY = viewHeight - 80;
        } else if (transY < minY) {
            transY = minY;
        }

        values[Matrix.MTRANS_X] = transX;
        values[Matrix.MTRANS_Y] = transY;
        m.setValues(values);
    }*/
    /*
     * --------------------------------------------------------------------------
     * Method: midPoint Parameters: PointF object, MotionEvent Returns: void
     * Description: calculates the midpoint between the two fingers
     * ------------------------------------------------------------
     */

    private void midPoint(PointF point, MotionEvent event)
    {
        float x = event.getX(0) + event.getX(1);
        float y = event.getY(0) + event.getY(1);
        point.set(x / 2, y / 2);
    }

    /** Show an event in the LogCat view, for debugging */
    private void dumpEvent(MotionEvent event)
    {
        String names[] = { "DOWN", "UP", "MOVE", "CANCEL", "OUTSIDE","POINTER_DOWN", "POINTER_UP", "7?", "8?", "9?" };
        StringBuilder sb = new StringBuilder();
        int action = event.getAction();
        int actionCode = action & MotionEvent.ACTION_MASK;
        sb.append("event ACTION_").append(names[actionCode]);

        if (actionCode == MotionEvent.ACTION_POINTER_DOWN || actionCode == MotionEvent.ACTION_POINTER_UP)
        {
            sb.append("(pid ").append(action >> MotionEvent.ACTION_POINTER_INDEX_MASK);
            sb.append(")");
        }

        sb.append("[");
        for (int i = 0; i < event.getPointerCount(); i++)
        {
            sb.append("#").append(i);
            sb.append("(pid ").append(event.getPointerId(i));
            sb.append(")=").append((int) event.getX(i));
            sb.append(",").append((int) event.getY(i));
            if (i + 1 < event.getPointerCount())
                sb.append(";");
        }

        sb.append("]");
        Log.d("Touch Events ---------", sb.toString());
    }
    public void downloadImage(View view){
        //Download Image
        if(selectedImageURL!=null) {
            Toast.makeText(getApplicationContext(), getString(R.string.downloadingNow), Toast.LENGTH_SHORT).show();
            Picasso.with(ShowImageFullScreen.this).load(selectedImageURL).into(target);

        }
        else {
            Log.v(TAG, "downloadImage");
            Toast.makeText(getApplicationContext(), getString(R.string.internalServerError), Toast.LENGTH_SHORT).show();
        }
    }
    public void shareImage(View view){
        //Share Image
        GeneralHelper.verifyStoragePermissions(this);
        ProgressDialog progressBar = new ProgressDialog(this);
        progressBar.setCancelable(true);
        progressBar.setMessage(getString(R.string.downloadingNow));
        progressBar.show();
        try{
            //progressBar.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            //progressBar.setProgress(0);
            //progressBar.setMax(100);
        if(selectedImageURL!=null) {
                Log.v(TAG, selectedImageURL);
                //Uri imageUri = Uri.parse(selectedImageURL);
                Uri bmpUri = GeneralHelper.getLocalBitmapUri(fullScreenImage);
                if (bmpUri != null) {
                    Intent shareIntent = new Intent();
                    shareIntent.setAction(Intent.ACTION_SEND);
                    shareIntent.putExtra(Intent.EXTRA_TEXT, getString(R.string.ShortURL));
                    shareIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Downloaded from " + getString(R.string.ShortURL));
                    shareIntent.putExtra(Intent.EXTRA_STREAM, bmpUri);
                    shareIntent.setType("*/*");
                    shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    startActivity(Intent.createChooser(shareIntent, getString(R.string.share)));
                } else {
                    Log.v(TAG, "shareImage is null");
                    Toast.makeText(getApplicationContext(), getString(R.string.internalServerError), Toast.LENGTH_SHORT).show();
                }
            }else{
                Log.v(TAG, "shareImage");
                Toast.makeText(getApplicationContext(), getString(R.string.internalServerError), Toast.LENGTH_SHORT).show();
            }
        }catch(Exception e){
            e.printStackTrace();
            Toast.makeText(getApplicationContext(), getString(R.string.internalServerError), Toast.LENGTH_SHORT).show();
        }
        progressBar.dismiss();
    }
    private Target target = new Target() {
        @Override
        public void onBitmapLoaded(final Bitmap bitmap, Picasso.LoadedFrom from) {
            new Thread(new Runnable() {
                @Override
                public void run() {

                    File file = new File(
                            Environment.getExternalStorageDirectory().getPath()
                                    + "/TamilHistoricalFacts/images/"+GeneralHelper.getRandomNumber(999999999)+".jpg");
                    try {
                        if(!file.getParentFile().mkdirs()) {
                            Log.v(TAG,"Folder does not exists");
                        }
                        file.createNewFile();
                        FileOutputStream ostream = new FileOutputStream(file);
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, ostream);
                        ostream.close();
                    }
                    catch (Exception e) {
                        e.printStackTrace();
                        handler.post(new Runnable() {
                            public void run() {
                                Toast toast = Toast.makeText(getApplicationContext(), getString(R.string.gpsError), Toast.LENGTH_LONG);
                                toast.show();
                            }
                        });
                    }
                }
            }).start();
        }

        @Override
        public void onBitmapFailed(Drawable errorDrawable) {
            Log.v(TAG,"onBitmapFailed");
            Toast.makeText(getApplicationContext(),getString(R.string.internalServerError),Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onPrepareLoad(Drawable placeHolderDrawable) {
            //Toast.makeText(getApplicationContext(),getString(R.string.internalServerError),Toast.LENGTH_SHORT).show();
        }
    };


}
