package com.myhistory.android.ta.events;

/**
 * Created by NA112077 on 10/28/2016.
 * Last Edited by NA112077 on 10/28/2016
 * Project Name : historicalfacts_coreandroid_repo
 * Package Name : com.myhistory.android.ta.events
 */

public class LanguageChange {
    public boolean isLanguageChanged;
    public boolean isSplashEntry;

    public LanguageChange(boolean isLanguageChanged, boolean isSplashEntry) {
        this.isLanguageChanged = isLanguageChanged;
        this.isSplashEntry = isSplashEntry;
    }
}
