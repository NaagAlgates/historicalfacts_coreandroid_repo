package com.myhistory.android.ta.bean;

/**
 * Created by Nagaraj.A on 9/2/2015.
 * Last Modified by : Nagaraj.A
 * Package Name : com.myhistory.android.ta.bean
 * Project Name : TamilHistoricalFacts
 */
public class DisplayStoryDetails {
    private String story_id;
    private String tam_title;
    private String sno;

    private String tam_content;
    private String display_pic;
    private String inserted_datetime;
    private String geo_location;
    private String read_status;
    private String social_status;


    public String getSno() {
        return sno;
    }

    public void setSno(String sno) {
        this.sno = sno;
    }

    public String getTam_content() {
        return tam_content;
    }

    public void setTam_content(String tam_content) {
        this.tam_content = tam_content;
    }

    public String getRead_status() {
        return read_status;
    }

    public void setRead_status(String read_status) {
        this.read_status = read_status;
    }

    public String getSocial_status() {
        return social_status;
    }

    public void setSocial_status(String social_status) {
        this.social_status = social_status;
    }

    public String getStory_id() {
        return story_id;
    }

    public void setStory_id(String story_id) {
        this.story_id = story_id;
    }

    public String getTam_title() {
        return tam_title;
    }

    public void setTam_title(String tam_title) {
        this.tam_title = tam_title;
    }

    public String getDisplay_pic() {
        return display_pic;
    }

    public void setDisplay_pic(String display_pic) {
        this.display_pic = display_pic;
    }

    public String getInserted_datetime() {
        return inserted_datetime;
    }

    public void setInserted_datetime(String inserted_datetime) {
        this.inserted_datetime = inserted_datetime;
    }

    public String getGeo_location() {
        return geo_location;
    }

    public void setGeo_location(String geo_location) {
        this.geo_location = geo_location;
    }

    @Override
    public String toString()
    {
        return "DisplayStoryDetails: story_id = ["+getStory_id()+"] tam_title = ["+getTam_title()+"] " +
                "display_pic = ["+getDisplay_pic()+"] inserted_datetime = ["+getInserted_datetime()+"]" +
                " geo_location = ["+getGeo_location()+"] ReadStatus = ["+getRead_status()+"] " +
                "SocialStatus = ["+ getSocial_status()+"] Tamil Content = ["+getTam_content()+"]" +
                " Sno = ["+getSno()+"]";
    }
}
