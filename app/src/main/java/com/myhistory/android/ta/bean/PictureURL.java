package com.myhistory.android.ta.bean;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Nagaraj.A on 8/27/2015.
 * Last Modified by : Nagaraj.A
 * Package Name : com.myhistory.android.ta.bean
 * Project Name : TamilHistoricalFacts
 */

public class PictureURL
{
    @SerializedName("IsHomeImage")
    private String IsHomeImage;
    @SerializedName("PictureURL")
    private String PictureURL;

    public String getIsHomeImage ()
    {
        return IsHomeImage;
    }

    public void setIsHomeImage (String IsHomeImage)
    {
        this.IsHomeImage = IsHomeImage;
    }

    public String getPictureURL ()
    {
        return PictureURL;
    }

    public void setPictureURL (String PictureURL)
    {
        this.PictureURL = PictureURL;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [IsHomeImage = "+getIsHomeImage()+", PictureURL = "+getPictureURL()+"]";
    }
}