package com.myhistory.android.ta;

import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Handler;
import android.widget.Toast;

import com.myhistory.android.ta.activity.MainActivity;
import com.myhistory.android.ta.events.LanguageChange;
import com.myhistory.android.ta.events.SetLanguage;
import com.myhistory.android.ta.utility.DatabaseMenuHandler;
import com.myhistory.android.ta.utility.Helper;
import com.myhistory.android.ta.utility.LocaleUtils;
import com.myhistory.android.ta.utility.Logger;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.Locale;

/**
 * Created by NA112077 on 10/27/2016.
 * Last Edited by NA112077 on 10/27/2016
 * Project Name : historicalfacts_coreandroid_repo
 * Package Name : com.myhistory.android.ta.utility
 */

public class App extends Application {

    private Locale locale = null;private
    static String TAG = App.class.getSimpleName();
    private static App singletonInstance;
    SharedPreferences pref;

    public static App get() {
        return singletonInstance;
    }
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (locale != null) {
            LocaleUtils.updateConfig(this, newConfig);
            /*Locale.setDefault(locale);
            Configuration config = new Configuration(newConfig);
            config.locale = locale;
            getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());*/
        }
    }
    @Override
    public void onCreate() {
        super.onCreate();
        singletonInstance = this;
        EventBus.getDefault().register(this);
        //LocaleUtils.setLocale(new Locale("en"));
        pref =getSharedPreferences(Helper.APP_SHARED_PREF, Context.MODE_PRIVATE);
        LocaleUtils.updateConfig(this, getBaseContext().getResources().getConfiguration());
    }
    public void changeLang(String lang) {
        /*if (!"".equals(lang) && !config.locale.getLanguage().equals(lang)
                && !(pref.getString(this.getString(R.string.locale_lang),"")).equals(lang)) {*/
        //if(!getLang().equals(lang)){
            SharedPreferences.Editor ed = pref.edit();
            ed.putString(getString(R.string.locale_lang), lang);
            ed.apply();

        setLocale(lang);
        //}
    }

    public void setLocale(String lang){
        Configuration config = getBaseContext().getResources().getConfiguration();
        locale = new Locale(lang);
        Locale.setDefault(locale);
        Configuration conf = new Configuration(config);
        conf.locale = locale;
        getBaseContext().getResources().updateConfiguration(conf, getBaseContext().getResources().getDisplayMetrics());
    }

    public String getLang(){
        return pref.getString(this.getString(R.string.locale_lang),null);
        //return PreferenceManager.getDefaultSharedPreferences(this).getString(this.getString(R.string.locale_lang), "");
    }
    @Subscribe
    public void setLanguage(SetLanguage languageChanged){
        String sLanguage = getSelectedLanguageCode(languageChanged.selectedLanguage);
        changeLang(sLanguage);
        Toast.makeText(get(),"You new language is set to : "+languageChanged.selectedLanguage,Toast.LENGTH_SHORT).show();
        EventBus.getDefault().post(new LanguageChange(true,languageChanged.isSplash));
    }
    @Subscribe
    public void languageChanged(LanguageChange languageChanged){
        if(languageChanged.isLanguageChanged){
            Logger.v(TAG,"Language Changed");
            if(languageChanged.isLanguageChanged) {
                openMain();
            }
        }else{
            String sLanguage=App.get().getLang();
            if(sLanguage==null || sLanguage.length()<1){
                //set default language
                App.get().changeLang("ta");
                Logger.v(TAG,"Language set to default");
                //if(languageChanged.isLanguageChanged) {
                openMain();
                //}
            }
        }
    }
    public String getSelectedLanguageCode(String selectedLanguage) {
        int i = -1;
        for (String cc: getResources().getStringArray(R.array.language_key)) {
            i++;
            if (cc.equals(selectedLanguage))
                break;
        }
        return getResources().getStringArray(R.array.language_value)[i];
    }

    private void openMain(){
        int SPLASH_TIME_OUT = Helper.SPLASH_TIME_OUT;
        //clear db if required
        DatabaseMenuHandler databaseMenuHandler =new DatabaseMenuHandler(get());
        databaseMenuHandler.deleteTables();
        Logger.v(TAG,"DB Cleared Successfully");
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                Intent intent = new Intent("finish_main_activity");
                sendBroadcast(intent);
                Intent i = new Intent(App.get(), MainActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
                Logger.v(TAG,"Timer over");
            }
        }, SPLASH_TIME_OUT);


    }


}