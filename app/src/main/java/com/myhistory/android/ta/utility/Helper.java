package com.myhistory.android.ta.utility;

import android.Manifest;

/**
 * Created by Nagaraj.A on 8/17/2015.
 * Last Modified by : Nagaraj.A
 * Package Name : com.myhistory.android.ta.utility
 * Project Name : Tamil Historical Facts
 */
public interface Helper {
    String englishURL="http://test-dot-historical-facts.appspot.com/client/index.htm?lang=English";
    String tamilURL="http://test-dot-historical-facts.appspot.com/client/index.htm?lang=Tamil";
    String englishOffline="file:///android_asset/index.htm?lang=English";
    String tamilOffline="file:///android_asset/index.htm?lang=Tamil";
    String jsonURL_getBriefStory="http://historical-facts.appspot.com/server/index.php/v2/api/stories/STORY_COUNT";
    //String jsonURL_getBriefStory="http://historical-facts.appspot.com/server/index.php/v2/api/stories/STORY_COUNT/LANGUAGE";
    String imgDefaultURL="https://storage.googleapis.com/historical-facts.appspot.com/images/";
    String URL_REGISTER_DEVICE_ID="http://historical-facts.appspot.com/gcm/register.php";
    String URL_DONATION="http://historical-facts.appspot.com/about/index.htm";
    char separatorChar = ',';
    String DEFAULT_GCM_SENDER_ID ="533560921395";
    int SPLASH_TIME_OUT=3000;

    String APP_SHARED_PREF="TAMIL_HISTORICAL_FACT_PREF";
    String SORTING_TYPE="SORTING";
    String GCM_REG_ID="GCM_REG_ID";
    String PREF_IS_FIRST_TIME_USER_MAIN="FIRST_TIME_USER_MAIN";
    String PREF_IS_FIRST_TIME_USER_SPLASH="FIRST_TIME_USER_SPLASH";
    String PREF_HEAVY_LOAD = "HEAVY_LOAD";

    String fBIDNaag = "100001904703470";
    String fBIDVenki = "1601062969";
    String fBIDSasi = "100001599881709";

     String linkedInIDNaag = "61608250";
     String linkedInIDVenki = "95985824";
     String linkedInIDSasi = "";

     String linkedInURLNaag = "https://www.linkedin.com/in/ausmnaag";
     String linkedInURLVenki = "http://in.linkedin.com/pub/venkatesh-mohanram/28/18b/428";
     String linkedInURLSasi = "https://www.linkedin.com/in/";

    String WebsiteURLNaag = "http://naagsblog.blogspot.in/";
    String WebsiteURLVenki ="http://venkateshbook.blogspot.in/";
    String WebsiteURLSasi = "http://www.varalaatrupudhayal.com/";

    String gpNaag = "106193078437170677095";
    String gpVenki = "109231061390628172861";
    String gpSasi = "109997202204393164213";

    String twitterNaag = "Mr_Naag";
    String twitterVenki = "";
    String twitterSasi = "";

    String IstNaag ="naag.algates";
    String IstVenki="";
    String IstSasi ="";

    String[] _contactMailIDs={"nagharaj.cse@gmail.com,venkatesh.mohanram@gmail.com"};
    String[] contactMailID={"tamilhistoricalfacts@gmail.com"};

    String _contactUsMailContent ="Hi Team, \n\n" +
            "\t\t\tI want to get it in touch with you.\n" +
            "My Number : \n\n" +
            "Please feel free to contact me during 9AM to 9 PM." +
            "\n\n\n\n\n\n General Details\n\n" +
            "MOBILE_DETAILS\n\n";

    String _contactUsMailSubject ="Contact Request from APP_NAME";

    String DATABASE_NAME="HISTORICAL_FACTS_DB_ANDROID";
    int DATABASE_VERSION=1;
    int REQUEST_EXTERNAL_STORAGE = 1;
    String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

}
