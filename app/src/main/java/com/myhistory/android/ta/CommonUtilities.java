
package com.myhistory.android.ta;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.util.Log;
import com.google.android.gcm.GCMRegistrar;
import com.myhistory.android.ta.utility.Helper;

public final class CommonUtilities
{

	//Activity _activity;
	Context _activity;
	//TextView mDisplay;
	//AsyncTask<Void, Void, Void> mRegisterTask;
	
	String regId = "";
	
	String TAG = "GCM CommonUtilities";
    static final String DISPLAY_MESSAGE_ACTION ="com.myhistory.android.ta";
    static final String EXTRA_MESSAGE = "message";
    public CommonUtilities (Activity act)
    {
    	this._activity = act;
    }
    static void displayMessage (Context context, String message)
    {
        Intent intent = new Intent(DISPLAY_MESSAGE_ACTION);
        intent.putExtra(EXTRA_MESSAGE, message);
        context.sendBroadcast(intent);
    }

    public void gcmRegister()
    {
		
    	try {
		GCMRegistrar.checkDevice(_activity);
		GCMRegistrar.checkManifest(_activity);
		//mDisplay = new TextView(_activity);
		
		try {
			
			_activity.registerReceiver(mHandleMessageReceiver, new IntentFilter(DISPLAY_MESSAGE_ACTION));
			
		}
		catch (Exception e) {

			e.printStackTrace();
		}
		
		regId = GCMRegistrar.getRegistrationId(_activity);
		Log.d("activity is", "::" + _activity + " --- " + regId);
			SharedPreferences pref = _activity.getSharedPreferences(Helper.APP_SHARED_PREF, Context.MODE_PRIVATE);
			SharedPreferences.Editor editor = pref.edit();
			editor.putString(Helper.GCM_REG_ID, regId);
			editor.apply();
		
		if (regId.equals("")) {
			// Automatically registers application on startup.
			GCMRegistrar.register(_activity, Helper.DEFAULT_GCM_SENDER_ID);
			 Log.d("NOT REGISTERED", "::NOT REGISTERED");
		}
		else {
			if (GCMRegistrar.isRegisteredOnServer(_activity)) {
				Log.d(TAG,"ALREADY REGISTERED");
				final Context context = _activity;
			}
			else {
				Log.d(TAG,"ERROR");
				  
				  final Context context = _activity;
			}
		}
	}// try end
	catch (Exception e) {
		e.printStackTrace();

	}

}

	


	
    
/*private void checkNotNull(Object reference, String name)
{
	if (reference == null) {
		throw new NullPointerException("Error in File Config : "+name);
	}
}*/


private BroadcastReceiver mHandleMessageReceiver = new BroadcastReceiver()
{
    @Override
	public void onReceive(Context context, Intent intent)
	{
		// String newMessage = intent.getExtras().getString(EXTRA_MESSAGE);
		// mDisplay.append(newMessage + "\n");
	}
};

public void destroyGcm()
{
    try {
//	    if (mRegisterTask != null) {
//		   mRegisterTask.cancel(true);
//	    }
		if(mHandleMessageReceiver!=null) {
			_activity.unregisterReceiver(mHandleMessageReceiver);
			GCMRegistrar.onDestroy(_activity);
			mHandleMessageReceiver=null;
		}

    }
	catch(Exception e) {
	   e.printStackTrace();
    }

}

}
