package com.myhistory.android.ta.fragment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.demievil.library.RefreshLayout;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.myhistory.android.ta.R;
import com.myhistory.android.ta.activity.HelpPullDownToRefresh;
import com.myhistory.android.ta.activity.ScreenSlideActivity;
import com.myhistory.android.ta.adapter.DisplayStoryAdapter;
import com.myhistory.android.ta.async.DownloadStory;
import com.myhistory.android.ta.bean.DisplayStoryDetails;
import com.myhistory.android.ta.utility.AskLanguage;
import com.myhistory.android.ta.utility.DatabaseMenuHandler;
import com.myhistory.android.ta.utility.GeneralHelper;
import com.myhistory.android.ta.utility.Helper;
import com.myhistory.android.ta.utility.Logger;
import com.twotoasters.jazzylistview.JazzyListView;

import java.util.List;


/**
 * Created by Nagaraj.A on 8/27/2015.
 * Last Modified by : Nagaraj.A
 * Package Name : com.myhistory.android.ta.fragment
 * Project Name : TamilHistoricalFacts
 */
public class LoadStoriesFragment extends Fragment {
    View rootView;
    RefreshLayout mRefreshLayout;
    JazzyListView mListView;
    View footerLayout;
    TextView textMore;
    //TextView textMessage;
    ProgressBar loadProgressBar;
    DatabaseMenuHandler databaseMenuHandler;
    List<DisplayStoryDetails>  detailsList;
    int dataCount,currentCount,storyTotalCount;
    private static String TAG="LoadStoriesFragment";
    private String sortType;
    SharedPreferences pref;
    AdView mAdView;
    DownloadStory downloadStory;
    private CoordinatorLayout coordinatorLayout;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_load_stories,container,false);
        return rootView;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        Log.v(TAG, "onSaveInstanceState");
        super.onSaveInstanceState(outState);
    }
    @Override
    public void onStart() {
        Log.v(TAG, "onStart");
        super.onStart();


    }
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        Log.i(TAG, "onActivityCreated");
        super.onActivityCreated(savedInstanceState);
        appLogic();

    }

    private void appLogic(){
        try{
            coordinatorLayout = (CoordinatorLayout) getActivity().findViewById(R.id.coordinatorLayout);
            pref = getActivity().getSharedPreferences(Helper.APP_SHARED_PREF, 0);
            //MainActivity activity = (MainActivity) getActivity();
            sortType = pref.getString(Helper.SORTING_TYPE,"0"); //activity.getSortType();
            databaseMenuHandler = new DatabaseMenuHandler(getActivity());
            /*mAdView = (AdView)  getActivity().findViewById(R.id.ad_view);
            AdRequest adRequest = new AdRequest.Builder()
                    .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                    .build();
            mAdView.loadAd(adRequest);*/
            mRefreshLayout = (RefreshLayout) getActivity().findViewById(R.id.swipe_container);
            mListView = (JazzyListView ) getActivity().findViewById(R.id.list);
            footerLayout = getActivity().getLayoutInflater().inflate(R.layout.listview_footer, null);
            //textMessage = (TextView) getActivity().findViewById(R.id.textMessage);
            mListView.addFooterView(footerLayout);
            mRefreshLayout.setChildView(mListView);
            loadProgressBar = (ProgressBar) getActivity().findViewById(R.id.load_progress_bar);
            textMore = (TextView) getActivity().findViewById(R.id.text_more);
            textMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    simulateLoadingData();
                }
            });

            mRefreshLayout.setColorSchemeResources(R.color.holo_blue_bright, R.color.holo_blue_dark,
                    R.color.holo_blue_light, R.color.holo_green_dark
                    , R.color.holo_green_light, R.color.holo_orange_dark,
                    R.color.holo_orange_light, R.color.holo_purple);
            //mRefreshLayout.setRefreshing(true);
            /*mRefreshLayout.post(new Runnable() {
                @Override
                public void run() {
                    mRefreshLayout.setRefreshing(true);
                }
            });*/
            if (pref.getBoolean(Helper.PREF_IS_FIRST_TIME_USER_MAIN, true)) {
                SharedPreferences.Editor editor = pref.edit();
                editor.putBoolean(Helper.PREF_IS_FIRST_TIME_USER_MAIN, false);
                editor.apply();
                Intent a = new Intent(getActivity(), HelpPullDownToRefresh.class);
                getActivity().startActivity(a);
                /*AskLanguage askLanguage = new AskLanguage(getActivity());
                askLanguage.show();*/
            }
            loadStory();
            /*mRefreshLayout.post(new Runnable() {
                @Override
                public void run() {
                    mRefreshLayout.setRefreshing(false);
                }
            });*/
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onPause() {
        Log.v(TAG, "onPause");
        super.onPause();
    }

    @Override
    public void onResume() {
        Log.v(TAG, "onResume");
        super.onResume();
    }

    @Override
    public void onStop() {
        Log.v(TAG,"onStop");
        super.onStop();
    }

    @Override
    public void onDestroyView() {
        Log.v(TAG, "onDestroyView");
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        Log.v(TAG, "onDestroy");
        super.onDestroy();
    }

    @Override
    public void onDetach() {
        Log.v(TAG, "onDetach");
        super.onDetach();
    }
    private void loadStory(){

        mRefreshLayout.setOnRefreshListener(new RefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // start to refresh
                //mRefreshLayout.setRefreshing(true);
                pref = getActivity().getSharedPreferences(Helper.APP_SHARED_PREF, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = pref.edit();
                editor.putBoolean(Helper.PREF_HEAVY_LOAD, true);
                editor.apply();
                loadData();
                //mRefreshLayout.setRefreshing(false);
            }
        });
        mRefreshLayout.setOnLoadListener(new RefreshLayout.OnLoadListener() {
            @Override
            public void onLoad() {
                // start to load
                /*mRefreshLayout.setLoading(true); //this will jump to bottom anf callback OnLoadListener.onLoad()
                mRefreshLayout.setLoading(false); //this will reset load more coordinates*/
                simulateLoadingData();
            }
        });
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                try {
                    String clickedStorySno ;//detailsList.get(position).getSno();
                    switch (sortType){
                        case "1":
                            //விரும்பியவை
                        case "2":
                            //நட்சத்திரமிட்டவை
                        case "3":
                            //வழிசெலுத்தல் உள்ளவை
                            clickedStorySno = ""+position;
                            break;
                        default:
                            //அனைத்தும்
                            clickedStorySno=detailsList.get(position).getSno();
                            break;
                    }
                    (new DisplayStoryAdapter(getContext(), detailsList)).notifyDataSetChanged();
                    Intent intent = new Intent(getActivity(), ScreenSlideActivity.class);
                    intent.putExtra("TOTAL_STORY_COUNT", storyTotalCount);
                    Logger.v(TAG,clickedStorySno);
                    intent.putExtra("CLICKED_SNO", clickedStorySno);
                    startActivity(intent);
                    //startActivity(new Intent(getActivity(), ScreenSlideActivity.class));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        //mRefreshLayout.setRefreshing(true);
        loadData();
        //mRefreshLayout.setRefreshing(false);
    }
    private void loadData(){
        try {
            //dataCount = databaseMenuHandler.getCountFromTable("SELECT COUNT(*) FROM TBL_STORY_DETAILS");
            //Log.v(TAG, "" + dataCount);
            //if (dataCount == 0) {
            //pref = getActivity().getSharedPreferences(Helper.APP_SHARED_PREF, 0);
            if (pref.getBoolean(Helper.PREF_HEAVY_LOAD, true)) {
                getActivity();
                pref = getActivity().getSharedPreferences(Helper.APP_SHARED_PREF, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = pref.edit();
                editor.putBoolean(Helper.PREF_HEAVY_LOAD, false);
                editor.apply();

                getData();
            /*if(GeneralHelper.checkInternetConnection(getActivity())) {
                try {
                    String downloadURL = Helper.jsonURL_getBriefStory;
                    downloadURL = downloadURL.replace("STORY_COUNT",databaseMenuHandler.getLastStoryID("select story_id from TBL_STORY_DETAILS ORDER BY story_id DESC LIMIT 1"));
                    //Toast.makeText(getActivity(),getString(R.string.downloadingNow),Toast.LENGTH_LONG).show();
                    new DownloadStory(getActivity(),downloadURL,mListView,mRefreshLayout,new FragmentCallback() {

                        @Override
                        public void onTaskDone() {
                            mRefreshLayout.setRefreshing(true);
                            setDataInListView();
                            mRefreshLayout.setRefreshing(false);
                        }
                    }).execute();

                }catch (Exception e){
                    e.printStackTrace();
                }
            }
            else
                Toast.makeText(getActivity(),getActivity().getString(R.string.IntDownInfoText_p1),Toast.LENGTH_SHORT).show();*/
            } else {
                try {
                    setDataInListView();
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    private void getData(){
        if(GeneralHelper.checkInternetConnection(getActivity())) {
            try {
                //mListView.setVisibility(View.VISIBLE);
                //textMessage.setVisibility(View.INVISIBLE);
                String downloadURL = Helper.jsonURL_getBriefStory;
                downloadURL = downloadURL.replace("STORY_COUNT",databaseMenuHandler.getLastStoryID("select story_id from TBL_STORY_DETAILS ORDER BY story_id DESC LIMIT 1"));
                //if(downloadStory.getStatus() != AsyncTask.Status.RUNNING ) {
                    //Toast.makeText(getActivity(),getString(R.string.downloadingNow),Toast.LENGTH_LONG).show();
                    downloadStory = (DownloadStory) new DownloadStory(getActivity(), downloadURL, mListView, mRefreshLayout, new FragmentCallback() {

                        @Override
                        public void onTaskDone() {
                            //mRefreshLayout.setRefreshing(true);
                            setDataInListView();
                            //mRefreshLayout.setRefreshing(false);
                        }
                    }).execute();
                /*}else{
                    Toast.makeText(getActivity(),getText(R.string.loading),Toast.LENGTH_SHORT).show();
                }*/

            }catch (Exception e){
                e.printStackTrace();
            }
        }
        else {
            //Toast.makeText(getActivity(), getActivity().getString(R.string.IntDownInfoText_p1), Toast.LENGTH_SHORT).show();
            Snackbar snackbar = Snackbar
                    .make(coordinatorLayout, getActivity().getString(R.string.IntDownInfoText_p1), Snackbar.LENGTH_LONG);
            snackbar.setActionTextColor(Color.RED);
            snackbar.show();

            //setDataInListView();
            /*textMessage.setVisibility(View.VISIBLE);
            textMessage.bringToFront();
            textMessage.invalidate();*/
            mRefreshLayout.post(new Runnable() {
                @Override
                public void run() {
                    mRefreshLayout.setRefreshing(false);
                }
            });
        }
    }
    private void setDataInListView(){
        try {
            //dataCount = databaseMenuHandler.getCountFromTable("SELECT COUNT(*) FROM TBL_STORY_DETAILS");
            switch (sortType) {
                case "1":
                    //விரும்பியவை
                    dataCount = databaseMenuHandler.getCountFromTable("SELECT COUNT(*) FROM TBL_STORY_DETAILS WHERE social_status = 1");
                    break;
                case "2":
                    //நட்சத்திரமிட்டவை
                    dataCount = databaseMenuHandler.getCountFromTable("SELECT COUNT(*) FROM TBL_STORY_DETAILS WHERE download_status = 1");
                    break;
                case "3":
                    //வழிசெலுத்தல் உள்ளவை
                    dataCount = databaseMenuHandler.getCountFromTable("SELECT COUNT(*) FROM TBL_STORY_DETAILS WHERE geo_location <> 0");
                    break;
                default:
                    //அனைத்தும்
                    dataCount = databaseMenuHandler.getCountFromTable("SELECT COUNT(*) FROM TBL_STORY_DETAILS");
                    break;
            }
            storyTotalCount = dataCount;
            if(storyTotalCount==0){
                pref = getActivity().getSharedPreferences(Helper.APP_SHARED_PREF, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = pref.edit();
                editor.putBoolean(Helper.PREF_HEAVY_LOAD, true);
                editor.apply();
                loadData();
            }else {
                textMore.setText(getActivity().getString(R.string.loadMore));
                currentCount = 1;
        /*currentCount = dataCount-9;
        if(currentCount<0)
            currentCount=0;*/
                int sortingFlag = 0;
                try {
                    sortingFlag = Integer.parseInt(sortType);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                detailsList = databaseMenuHandler.storyDetailsList(currentCount, 10, sortingFlag);
                if (detailsList != null) {
                    mListView.setAdapter(new DisplayStoryAdapter(getContext(), detailsList));
                    mListView.setVisibility(View.VISIBLE);
                } else {
                    //mListView.setVisibility(View.INVISIBLE);
                /*textMessage.setVisibility(View.VISIBLE);
                textMessage.bringToFront();
                textMessage.invalidate();*/
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    private void simulateLoadingData() {
        if (!(textMore.getText().toString().equalsIgnoreCase(getActivity().getString(R.string.completed)))) {
            textMore.setVisibility(View.GONE);
            loadProgressBar.setVisibility(View.VISIBLE);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    //getNewBottomData();
                    mRefreshLayout.setLoading(false);
                    currentCount=currentCount+10;
                    dataCount = currentCount +9;
                    /*if (dataCount < 1)
                        dataCount = 0;
                    currentCount = dataCount - 10;
                    if (currentCount < 1)
                        currentCount = 0;*/
                    if (dataCount <= storyTotalCount) {
                        int sortingFlag = 0;
                        try {
                            sortingFlag = Integer.parseInt(sortType);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        List<DisplayStoryDetails> details = databaseMenuHandler.storyDetailsList(currentCount, dataCount, sortingFlag);
                        for (int i = 0; i < details.size(); i++) {
                            detailsList.add(details.get(i));
                        }
                        (new DisplayStoryAdapter(getContext(), detailsList)).notifyDataSetChanged();
                        textMore.setVisibility(View.VISIBLE);
                        loadProgressBar.setVisibility(View.GONE);
                    } else {
                        textMore.setText(getActivity().getString(R.string.completed));
                        textMore.setVisibility(View.VISIBLE);
                        loadProgressBar.setVisibility(View.GONE);
                    }
                    //Toast.makeText(MainActivity.this, "Load Finished!", Toast.LENGTH_SHORT).show();
                }
            }, 1000);
        }
    }
    public interface FragmentCallback {
        void onTaskDone();
    }
}
