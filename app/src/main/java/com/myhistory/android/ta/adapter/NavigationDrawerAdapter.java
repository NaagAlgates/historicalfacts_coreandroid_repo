package com.myhistory.android.ta.adapter;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.myhistory.android.ta.App;
import com.myhistory.android.ta.R;
import com.squareup.picasso.Picasso;


/**
 * Created by Nagaraj.A on 8/17/2015.
 * Last Modified by : Nagaraj.A
 * Package Name : com.myhistory.android.ta.adapter
 * Project Name : Tamil Historical Facts
 */
public class NavigationDrawerAdapter extends ArrayAdapter<String> {
    private final Context tContext;
    private final String[] menuList;
    private final int[] iconList;

    public NavigationDrawerAdapter(Context c,String[] menuList,int[] iconList){
        super(c, R.layout.fragment_main, menuList);
        this.tContext = c;
        this.menuList=menuList;
        this.iconList=iconList;
    }
    private static class ViewHolder {
        TextView txtTitle;// = (TextView) rowView.findViewById(R.id.section_label);
        ImageView icon;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            LayoutInflater inflater =  (LayoutInflater) this.tContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.fragment_main, parent, false);
            //View rowView = inflater.inflate(R.layout.fragment_main, null, true);
            holder = new ViewHolder();
            holder.txtTitle = (TextView) convertView.findViewById(R.id.section_label);
            holder.icon=(ImageView) convertView.findViewById(R.id.section_icon);
            if(menuList[position].contains("VER_NO")) {
                try {
                    PackageInfo pInfo = tContext.getPackageManager().getPackageInfo(tContext.getPackageName(), 0);
                    menuList[position] = menuList[position].replace("VER_NO", pInfo.versionName);
                }catch(Exception e){
                    menuList[position] = menuList[position].replace("VER_NO", "0.1.1");
                    e.printStackTrace();
                }
            }
            holder.txtTitle.setText(menuList[position]);
            Picasso.with(App.get())
                    .load(iconList[position])
                    .placeholder(R.drawable.placeholder)
                    .error(R.drawable.pageload_error)
                    .into(holder.icon);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        return convertView;
    }
}
