package com.myhistory.android.ta.activity;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.TextView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.myhistory.android.ta.App;
import com.myhistory.android.ta.R;

/**
 * Created by Nagaraj.A on 10/13/2015.
 * Last Modified by : Nagaraj.A
 * Package Name : com.myhistory.android.ta.activity
 * Project Name : TamilHistoricalFacts
 */
public class HelpPullDownToRefresh extends Activity {
    static String TAG="HelpPullDownToRefresh";
    ImageView pullDownImage;
    Animation mAnimation;
    TextView helpPullDown,helpSection;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        App.get().setLocale(App.get().getLang());
        setContentView(R.layout.activity_help_pulldown);
        pullDownImage = (ImageView) findViewById(R.id.pullDownImage);
        helpPullDown = (TextView) findViewById(R.id.helpPullDown);
        helpSection = (TextView) findViewById(R.id.helpSection);
        YoYo.with(Techniques.FadeIn)
                .duration(3000)
                .playOn(findViewById(R.id.helpPullDown));
        YoYo.with(Techniques.SlideInLeft)
                .duration(5000)
                .playOn(findViewById(R.id.helpSection));

        mAnimation = new TranslateAnimation(
                TranslateAnimation.ABSOLUTE, 0f,
                TranslateAnimation.ABSOLUTE, 0f,
                TranslateAnimation.RELATIVE_TO_PARENT, 0f,
                TranslateAnimation.RELATIVE_TO_PARENT, 1.0f);
        mAnimation.setDuration(5000);
        mAnimation.setRepeatCount(20);
        //mAnimation.setRepeatMode(Animation.REVERSE);
        pullDownImage.setAnimation(mAnimation);
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Log.d(TAG, "Back Pressed");
        finish();
    }
    public void layoutClicked(View v){
        finish();
    }
}
