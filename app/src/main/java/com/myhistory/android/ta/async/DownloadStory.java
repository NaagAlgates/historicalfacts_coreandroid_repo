package com.myhistory.android.ta.async;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Handler;
import android.util.Log;
import android.widget.ListView;
import android.widget.Toast;

import com.demievil.library.RefreshLayout;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.myhistory.android.ta.R;
import com.myhistory.android.ta.bean.StoryDetails;
import com.myhistory.android.ta.fragment.LoadStoriesFragment;
import com.myhistory.android.ta.utility.DatabaseMenuHandler;
import com.myhistory.android.ta.utility.GeneralHelper;
import com.myhistory.android.ta.utility.Helper;
import com.myhistory.android.ta.utility.Logger;

/**
 * Created by Nagaraj.A on 8/27/2015.
 * Last Modified by : Nagaraj.A
 * Package Name : com.myhistory.android.ta.async
 * Project Name : TamilHistoricalFacts
 */

public class DownloadStory extends AsyncTask<Void, Void, Void> {
    private Context nContext = null;
    ListView loadStoryListView;
    String jsonURL="";
    String responseJSON="";
    private String TAG = "DownloadStory";
    DatabaseMenuHandler databaseMenuHandler;
    RefreshLayout mRefreshLayout;
    private LoadStoriesFragment.FragmentCallback mFragmentCallback;
    //private SharedPreferences pref;
    public DownloadStory(Context c, String jsonURL,ListView loadStoryListView,RefreshLayout mRefreshLayout,LoadStoriesFragment.FragmentCallback fragmentCallback){
        this.nContext = c;
        this.jsonURL = jsonURL;
        this.loadStoryListView = loadStoryListView;
        this.mRefreshLayout = mRefreshLayout;
        this.mFragmentCallback = fragmentCallback;
    }
    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        Log.v(TAG, "onPreExecute");
        databaseMenuHandler = new DatabaseMenuHandler(nContext);
        mRefreshLayout.setColorSchemeResources(R.color.holo_blue_bright, R.color.holo_blue_dark,
                R.color.holo_blue_light, R.color.holo_green_dark
                , R.color.holo_green_light, R.color.holo_orange_dark,
                R.color.holo_orange_light, R.color.holo_purple);
        mRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                mRefreshLayout.setRefreshing(true);
            }
        });
    }
    protected void onPostExecute(Void result) {
        Log.v(TAG, "onPostExecute");
        /*int dataCount = databaseMenuHandler.getCountFromTable("SELECT COUNT(*) FROM TBL_STORY_DETAILS");
        int currentCount = dataCount-9;
        int sortingFlag=0;
        List<DisplayStoryDetails> detailsList = databaseMenuHandler.storyDetailsList(currentCount,dataCount,sortingFlag);
        loadStoryListView.setAdapter(new DisplayStoryAdapter(nContext, detailsList));*/
        mRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                mRefreshLayout.setRefreshing(false);
            }
        });
        mFragmentCallback.onTaskDone();
    }
    @Override
    protected Void doInBackground(Void... params) {
        try {
            Log.v(TAG, "doInBackground");
            Gson gson = new GsonBuilder().create();
            Log.v(TAG, jsonURL);
            responseJSON = GeneralHelper.httpConnection(jsonURL);
            Log.v(TAG, responseJSON);
            StoryDetails[] storyDetailsList= gson.fromJson(responseJSON, StoryDetails[].class);
            String storyID,engTitle,tamTitle,engBrief,disPic,tamBrief,geoLocation,storyDate,picURL="";
            for (StoryDetails aStoryDetailsList : storyDetailsList) {
                storyID = aStoryDetailsList.getStoryID();
                //Log.v(TAG,storyID);
                engTitle = aStoryDetailsList.getEnglishTitle();
                //Log.v(TAG,engTitle);
                tamTitle = aStoryDetailsList.getTamilTitle();
                //Log.v(TAG,tamTitle);
                engBrief = aStoryDetailsList.getEnglishContent();
                //Log.v(TAG,engBrief);
                tamBrief = aStoryDetailsList.getTamilContent();
                //Log.v(TAG,tamBrief);
                geoLocation = aStoryDetailsList.getGeoLocation();
                //Log.v(TAG,geoLocation);
                storyDate = aStoryDetailsList.getStoryDate();
                //Log.v(TAG,storyDate);
                for (int j = 0; j < aStoryDetailsList.getPictureURL().size(); j++) {
                    disPic = aStoryDetailsList.getPictureURL().get(j).getPictureURL();
                    picURL = picURL + disPic + Helper.separatorChar;
                }
                picURL = GeneralHelper.removeLastCharacter(picURL, Helper.separatorChar);
                databaseMenuHandler.insertStoryDetails(storyID, engTitle, tamTitle, engBrief, tamBrief, picURL, storyDate, "", geoLocation);
                Logger.v(TAG,"Data Inserted successfully.");
                //databaseMenuHandler.close();
                //Log.v(TAG,picURL);
            }

            //System.out.println("Result - > " + storyDetailsList[0]);
        } catch (Exception e) {
            e.printStackTrace();

            Handler handler =  new Handler(nContext.getMainLooper());
            handler.post(new Runnable() {
                public void run() {
                    Toast.makeText(nContext, nContext.getString(R.string.internalServerError), Toast.LENGTH_SHORT).show();
                }
            });
        }
        return null;
    }
    protected void onProgressUpdate(Void... progress) {
        Log.v(TAG, "onProgressUpdate");
    }
}
