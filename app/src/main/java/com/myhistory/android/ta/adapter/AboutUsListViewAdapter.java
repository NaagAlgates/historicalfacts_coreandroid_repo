package com.myhistory.android.ta.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.myhistory.android.ta.R;
import com.myhistory.android.ta.utility.Helper;
import com.myhistory.android.ta.utility.OpenSocialMedia;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Nagaraj.A on 8/18/2015.
 * Last Modified by : Nagaraj.A
 * Package Name : com.myhistory.android.ta.adapter
 * Project Name : TamilHistoricalFacts
 */
public class AboutUsListViewAdapter extends BaseAdapter {
    private ArrayList<String> listName;
    private ArrayList<String> listDesc;
    private ArrayList<Integer> listDispImg;
    String TAG = "AboutUsAdapter";
    String fbOpenMsg = "";
    OpenSocialMedia opsm;
    Context context;

    public AboutUsListViewAdapter(Context c,ArrayList<String> listGetName,
                                  ArrayList<String> listGetDesc,ArrayList<Integer> listGetDispImg) {
        this.context = c;
        this.listName = listGetName;
        this.listDesc = listGetDesc;
        this.listDispImg = listGetDispImg;
        Log.i(TAG, "Context");

    }
    @Override
    public int getCount() {
        Log.i(TAG,String.valueOf(listName.size()));
        return listName.size();
    }

    @Override
    public Object getItem(int position) {
        Log.i(TAG,String.valueOf(listName.get(position)));
        return listName.get(position);
    }

    @Override
    public long getItemId(int position) {
        Log.i(TAG,"Return Position");
        return position;
    }
    public static class ViewHolder
    {
        public ImageView iFBIcon,iLinIcon,iGPlus,iDispImage,iBlogIcon;
        public TextView tName,tDesc;
    }
    @Override
    public View getView(int iListPosition, View arg1, ViewGroup parent) {
        Log.i(TAG,"GET_VIEW");
        ViewHolder view;
        fbOpenMsg = context.getString(R.string.fbclient);
        if(arg1==null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            opsm = new OpenSocialMedia(context);
            arg1 = inflater.inflate(R.layout.single_item_aboutus, parent, false);
            view = new ViewHolder();
            view.iBlogIcon = (ImageView) arg1.findViewById(R.id.AboutUsBlog);
            view.iFBIcon = (ImageView) arg1.findViewById(R.id.AboutUsFbIcon);
            view.iLinIcon = (ImageView) arg1.findViewById(R.id.AboutUsLinkedIn);
            view.iGPlus = (ImageView) arg1.findViewById(R.id.AboutUsGplus);
            view.iDispImage = (ImageView) arg1.findViewById(R.id.AboutUsImage);
            view.tName = (TextView) arg1.findViewById(R.id.AboutUsNameText);
            view.tDesc = (TextView) arg1.findViewById(R.id.AboutUsIntroText);

            view.iBlogIcon.setTag(iListPosition);
            view.iBlogIcon.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    //Intent fbIntent;
                    //Toast.makeText(context, fbOpenMsg, Toast.LENGTH_LONG).show();
                    Intent browserIntent=null;
                    switch ((Integer.valueOf(view.getTag().toString()))) {
                        case 0:
                            Log.i(TAG, "SASI_BLOG");
                            browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(Helper.WebsiteURLSasi));
                            break;
                        case 1:
                            Log.i(TAG, "VENK_BLOG");
                            browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(Helper.WebsiteURLVenki));
                            break;
                        case 2:
                            Log.i(TAG, "NAAG_BLOG");
                            browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(Helper.WebsiteURLNaag));
                            break;
                    }
                    if(browserIntent!=null)
                        context.startActivity(browserIntent);
                }
            });

            view.iFBIcon.setTag(iListPosition);
            view.iFBIcon.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    Intent fbIntent;
                    Toast.makeText(context, fbOpenMsg, Toast.LENGTH_LONG).show();
                    switch ((Integer.valueOf(view.getTag().toString())))
                    {
                        case 0:
                            Log.i(TAG,"SASI_FB");
                            //opsm.openFacebook(IConstants_Android.fBIDSasi);
                            fbIntent = opsm.openFacebook(context.getApplicationContext(), Helper.fBIDSasi);
                            context.startActivity(fbIntent);
                            break;
                        case 1:
                            Log.i(TAG,"VENK_FB");
                            fbIntent = opsm.openFacebook(context.getApplicationContext(),Helper.fBIDVenki);
                            context.startActivity(fbIntent);
                            //opsm.openFacebook(IConstants_Android.fBIDVenki);
                            break;
                        case 2:
                            Log.i(TAG,"NAAG_FB");
                            fbIntent = opsm.openFacebook(context.getApplicationContext(),Helper.fBIDNaag);
                            context.startActivity(fbIntent);
                            //opsm.openFacebook(IConstants_Android.fBIDNaag);
                            break;
                    }
                }
            });
            view.iLinIcon.setTag(iListPosition);
            view.iLinIcon.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    //Toast.makeText(context, "iLinIcon clicked for the row = " + view.getTag().toString(), Toast.LENGTH_SHORT).show();
                    switch ((Integer.valueOf(view.getTag().toString())))
                    {
                        case 0:
                            opsm.openLinkedIn(Helper.linkedInIDSasi, Helper.linkedInURLSasi);
                            break;
                        case 1:
                            opsm.openLinkedIn(Helper.linkedInIDVenki, Helper.linkedInURLVenki);
                            break;
                        case 2:
                            opsm.openLinkedIn(Helper.linkedInIDNaag, Helper.linkedInURLNaag);
                            break;
                    }
                }
            });
            view.iGPlus.setTag(iListPosition);
            view.iGPlus.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    //Toast.makeText(context, "iGPlus clicked for the row = " + view.getTag().toString(), Toast.LENGTH_SHORT).show();
                    switch ((Integer.valueOf(view.getTag().toString())))
                    {
                        case 0:
                            opsm.openGPlus(Helper.gpSasi);
                            break;
                        case 1:
                            opsm.openGPlus(Helper.gpVenki);
                            break;
                        case 2:
                            opsm.openGPlus(Helper.gpNaag);
                            break;
                    }
                }
            });

            view.iDispImage.setTag(iListPosition);
            /*view.iDispImage.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    //Toast.makeText(context, "iGPlus clicked for the row = " + view.getTag().toString(), Toast.LENGTH_SHORT).show();
                    Intent a = new Intent(context, ShowProfileImage.class);
                    switch ((Integer.valueOf(view.getTag().toString())))
                    {
                        case 0:
                            //opsm.openGPlus(Helper.gpSasi);
                            a.putExtra("SELECTED_PROFILE",R.drawable.sasi);
                            break;
                        case 1:
                            a.putExtra("SELECTED_PROFILE",R.drawable.venk);
                            //opsm.openGPlus(Helper.gpVenki);
                            break;
                        case 2:
                            a.putExtra("SELECTED_PROFILE",R.drawable.naag);
                            //opsm.openGPlus(Helper.gpNaag);
                            break;
                    }
                    context.startActivity(a);
                }
            });*/

            arg1.setTag(view);
        }
        else
        {
            view =   (ViewHolder) arg1.getTag();
        }
        /*Picasso.with(context.getApplicationContext())
                .load(R.drawable.facebook)
                .error(R.drawable.ic_launcher)
                .into(view.iFBIcon);
        Picasso.with(context.getApplicationContext())
                .load(R.drawable.linkedin)
                .error(R.drawable.ic_launcher)
                .into(view.iLinIcon);
        Picasso.with(context.getApplicationContext())
                .load(R.drawable.gplus)
                .error(R.drawable.ic_launcher)
                .into(view.iGPlus);*/
        Log.i(TAG+TAG,String.valueOf(listDispImg.get(iListPosition)));
        //int resID = context.getResources().getIdentifier(listDispImg.get(iListPosition),"id","com.myhistory.android.ta");
        //Log.i(TAG+TAG+TAG,String.valueOf(resID));
        Picasso.with(context.getApplicationContext())
                .load(listDispImg.get(iListPosition))
                .error(R.mipmap.ic_launcher)
                .into(view.iDispImage);

        view.tName.setText(listName.get(iListPosition));
        view.tDesc.setText(listDesc.get(iListPosition));
        return arg1;
    }
}
