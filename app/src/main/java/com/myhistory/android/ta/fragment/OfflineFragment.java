package com.myhistory.android.ta.fragment;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.myhistory.android.ta.R;
import com.myhistory.android.ta.essentials.GPSTracker;
import com.myhistory.android.ta.utility.Helper;


/**
 * Created by Nagaraj.A on 8/17/2015.
 * Last Modified by : Nagaraj.A
 * Package Name : com.myhistory.android.ta.fragment
 * Project Name : Tamil Historical Facts
 */
public class OfflineFragment extends Fragment {
    View rootView;
    private WebView loadDataWebView;
    private static String TAG="OnlineFragment";
    //private TextView noInternet;
    GPSTracker gps;
    int _backCounter=1;

    private Handler handler = new Handler(){
        @Override
        public void handleMessage(Message message) {
            switch (message.what) {
                case 0:
                    Log.i(TAG,"YesCalled");
                    webViewGoBack();
                    break;
            }
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_offline,container,false);
        return rootView;
    }
    @Override
    public void onAttach(Activity activity) {
        Log.v(TAG, "onAttach");
        super.onAttach(activity);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        Log.v(TAG, "onSaveInstanceState");
        super.onSaveInstanceState(outState);
        loadDataWebView.saveState(outState);
    }
    @Override
    public void onStart() {
        Log.v(TAG, "onStart");
        super.onStart();

    }
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        Log.i(TAG, "onActivityCreated");
        super.onActivityCreated(savedInstanceState);
        try{
            loadData(savedInstanceState);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onPause() {
        Log.v(TAG,"onPause");
        super.onPause();
    }

    @Override
    public void onResume() {
        Log.v(TAG, "onResume");
        super.onResume();
    }

    @Override
    public void onStop() {
        Log.v(TAG,"onStop");
        super.onStop();
    }

    @Override
    public void onDestroyView() {
        Log.v(TAG, "onDestroyView");
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        Log.v(TAG, "onDestroy");
        super.onDestroy();
    }

    @Override
    public void onDetach() {
        Log.v(TAG, "onDetach");
        super.onDetach();
    }
    public void loadData(Bundle savedInstanceState){
        //noInternet = (ImageView) getActivity().findViewById(R.id.NoInternetImage);
        loadDataWebView = (WebView) getActivity().findViewById(R.id.loadHTMLData);
        loadDataWebView.addJavascriptInterface(this, "interface");
        loadDataWebView.getSettings().setJavaScriptEnabled(true);
        loadDataWebView.setWebViewClient(new WebViewClient());
        loadDataWebView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        loadDataWebView.setWebChromeClient(new WebChromeClient());
        loadDataWebView.getSettings().setBuiltInZoomControls(true);
        loadDataWebView.getSettings().setDisplayZoomControls(false);
        loadDataWebView.setHorizontalScrollBarEnabled(false);
        loadDataWebView.setOnKeyListener(new View.OnKeyListener() {

            public boolean onKey(View v, int keyCode, KeyEvent event) {
                Log.i(TAG, "OnKEYDOWN");
                if ((keyCode == KeyEvent.KEYCODE_BACK)) {
                    Log.i(TAG, "BackWebViewCalled");
                    _backCounter++;
                    handler.sendEmptyMessage(_backCounter % 2);
                    return true;
                }
                return false;
            }

        });
        if (savedInstanceState != null)
            loadDataWebView.restoreState(savedInstanceState);
        else
            loadDataWebView.loadUrl(Helper.tamilOffline);
    }

    /*private class MyWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }

        @Override
        public void onReceivedError(WebView view, int errorCod, String description, String failingUrl) {
            //Set a dialog msg
            loadDataWebView.setVisibility(View.INVISIBLE);
            //noInternet.setVisibility(View.VISIBLE);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            loadDataWebView.loadUrl("javascript:updateFeature(20140701)");
        }
    }*/
    @JavascriptInterface // method will be accessible by your web page for android 4.3 or higher
    public void MapaCallfrmHTML(String siteLatitude, String siteLongitude) {
        double siteLati = Double.valueOf(siteLatitude);
        double siteLongi = Double.valueOf(siteLongitude);
        Log.i("Came", "Here");
        Log.i("SiteLATITUDE", String.valueOf(siteLati));
        Log.i("SiteLONGITUDE", String.valueOf(siteLongi));
        gps = new GPSTracker(getActivity().getApplicationContext());
        // check if GPS enabled
        if (gps.canGetLocation()) {
            Log.i("Came1", "Here1");
            double myLatitude = gps.getLatitude();
            double myLongitude = gps.getLongitude();

            Log.i("MyLATITUDE", String.valueOf(myLatitude));
            Log.i("MyLONGITUDE", String.valueOf(myLongitude));
            try {
                Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                        Uri.parse("http://maps.google.com/maps?   saddr=" + myLatitude + "," + myLongitude + "&daddr=" + siteLati + "," + siteLongi));
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addCategory(Intent.CATEGORY_LAUNCHER);
                intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
                startActivity(intent);
            } catch (ActivityNotFoundException e) {
                Toast.makeText(getActivity().getApplicationContext(), "Google Map is not installed", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(Intent.ACTION_VIEW).setData(Uri.parse("http://maps.google.com/maps?   saddr=" + myLatitude + "," + myLongitude + "&daddr=" + siteLati + "," + siteLongi)));
            } catch (Exception E) {
                E.printStackTrace();
            }

        } else {
            gps.showSettingsAlert();
        }
    }
    private void webViewGoBack()
    {
        Log.i(TAG,"WentBack");
        loadDataWebView.loadUrl("javascript:back()");
    }
    @JavascriptInterface
    public void closeWebView() {
        Log.i(TAG,"Back_Pressed");
        getActivity().finish();
    }
    @JavascriptInterface
    public void DownloadHistoryFunction(String storyID, String storyContent, int storyLanguage) {
        Log.d("OFFLINE_STORY_MGR", "Download Requested");
        //OfflineStoryManager.getInstance(getActivity().getApplicationContext()).downloadStory(Integer.parseInt(storyID));

    }
    @JavascriptInterface
    public void ShareHistoryFunction(String storySubject, String storyContent, String storyImageURL) {
        String shareTitle="";

        Intent i=new Intent(android.content.Intent.ACTION_SEND);
        i.setType("text/plain");
        i.putExtra(android.content.Intent.EXTRA_SUBJECT, storySubject);
        //i.putExtra(Intent.EXTRA_STREAM, Uri.parse(ImagePath));
        i.putExtra(android.content.Intent.EXTRA_TEXT, storyContent + "\n\n Similar article on: " + getText(R.string.ShortURL));
        startActivity(Intent.createChooser(i, shareTitle));
    }
}