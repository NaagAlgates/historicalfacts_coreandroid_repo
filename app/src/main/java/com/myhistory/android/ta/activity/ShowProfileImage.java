package com.myhistory.android.ta.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.myhistory.android.ta.App;
import com.myhistory.android.ta.R;

/**
 * Created by Nagaraj.A on 8/27/2015.
 * Last Modified by : Nagaraj.A
 * Package Name : com.myhistory.android.ta.activity
 * Project Name : TamilHistoricalFacts
 */
public class ShowProfileImage extends Activity {
    ImageView closeImage,displayImage;
    static String TAG="ShowProfileImage";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        App.get().setLocale(App.get().getLang());
        setContentView(R.layout.show_profile_image);
        closeImage = (ImageView) findViewById(R.id.closeImage);
        displayImage = (ImageView) findViewById(R.id.displayImage);
        closeImage.bringToFront();
        closeImage.invalidate();
        Intent intent = getIntent();
        int selectedProfile = intent.getIntExtra("SELECTED_PROFILE",0);
        if(selectedProfile!=0)
            displayImage.setBackgroundResource(selectedProfile);
        else
            displayImage.setBackgroundResource(R.mipmap.ic_launcher);
    }
    public void closeClicked(View v){
        finish();
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Log.d(TAG, "Back Pressed");
        finish();
    }
}
