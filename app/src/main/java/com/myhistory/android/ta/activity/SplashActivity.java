package com.myhistory.android.ta.activity;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.TextView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.myhistory.android.ta.App;
import com.myhistory.android.ta.R;
import com.myhistory.android.ta.utility.AskLanguage;
import com.myhistory.android.ta.utility.DatabaseMenuHandler;
import com.myhistory.android.ta.utility.Helper;
import com.myhistory.android.ta.utility.Logger;
import com.nineoldandroids.animation.Animator;
import com.nineoldandroids.animation.ArgbEvaluator;
import com.nineoldandroids.animation.ValueAnimator;

/**
 * Created by NA112077 on 10/27/2016.
 * Last Edited by NA112077 on 10/27/2016
 * Project Name : historicalfacts_coreandroid_repo
 * Package Name : com.myhistory.android.ta.activity
 */

public class SplashActivity extends Activity {
private static final String TAG=SplashActivity.class.getSimpleName();

    TextView splashText;// = (TextView) findViewById(R.id.splashText);
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        splashText = (TextView) findViewById(R.id.splashText);
        SharedPreferences pref = getSharedPreferences(Helper.APP_SHARED_PREF, 0);
        if (pref.getBoolean(Helper.PREF_IS_FIRST_TIME_USER_SPLASH, true)) {
            AskLanguage askLanguage = new AskLanguage(SplashActivity.this, true);
            askLanguage.show();
            SharedPreferences.Editor editor = pref.edit();
            editor.putBoolean(Helper.PREF_IS_FIRST_TIME_USER_SPLASH, false);
            editor.apply();
        }else{
            openMain();
        }
        registerReceiver(broadcast_reciever, new IntentFilter("finish_activity"));

    }
    private void openMain(){
        final int SPLASH_TIME_OUT = Helper.SPLASH_TIME_OUT;

        YoYo.with(Techniques.Flash)
        .duration(SPLASH_TIME_OUT)
        .interpolate(new AccelerateDecelerateInterpolator())
        .withListener(new Animator.AnimatorListener() {

            @Override
            public void onAnimationStart(Animator animation) {}

            @Override
            public void onAnimationEnd(Animator animation) {

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        DatabaseMenuHandler databaseMenuHandler =new DatabaseMenuHandler(App.get());
                        databaseMenuHandler.deleteTables();
                        Logger.v(TAG,"DB Cleared Successfully");
                        SharedPreferences pref = getSharedPreferences(Helper.APP_SHARED_PREF, Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = pref.edit();
                        editor.putBoolean(Helper.PREF_HEAVY_LOAD, true);
                        editor.apply();
                        Intent i = new Intent(App.get(), MainActivity.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(i);
                        Logger.v(TAG,"Timer over");
                    }
                }, 100);
            }

            @Override
            public void onAnimationCancel(Animator animation) {}

            @Override
            public void onAnimationRepeat(Animator animation) {}
        }).playOn(findViewById(R.id.splashText));


    }

    @Override
    public void onStart() {
        super.onStart();
    }
    @Override
    public void onStop() {
        super.onStop();
        if(broadcast_reciever!=null) {
            unregisterReceiver(broadcast_reciever);
        }
    }

    BroadcastReceiver broadcast_reciever = new BroadcastReceiver() {

        @Override
        public void onReceive(Context arg0, Intent intent) {
            String action = intent.getAction();
            if (action.equals("finish_activity")) {

                finish();
                // DO WHATEVER YOU WANT.
            }
        }
    };
}
