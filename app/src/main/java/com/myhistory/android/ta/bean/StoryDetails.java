package com.myhistory.android.ta.bean;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Nagaraj.A on 8/27/2015.
 * Last Modified by : Nagaraj.A
 * Package Name : com.myhistory.android.ta.bean
 * Project Name : TamilHistoricalFacts
 */
public class StoryDetails
{
    @SerializedName("EnglishTitle")
    private String EnglishTitle;
    @SerializedName("PictureURL")
    private List<PictureURL> PictureURL;
    @SerializedName("TamilContent")
    private String TamilContent;
    @SerializedName("GeoLocation")
    private String GeoLocation;
    @SerializedName("PlaceName")
    private String PlaceName;
    @SerializedName("TamilTitle")
    private String TamilTitle;
    @SerializedName("EnglishContent")
    private String EnglishContent;
    @SerializedName("StoryDate")
    private String StoryDate;
    @SerializedName("StoryID")
    private String StoryID;

    public String getEnglishTitle ()
    {
        return EnglishTitle;
    }

    public void setEnglishTitle (String EnglishTitle)
    {
        this.EnglishTitle = EnglishTitle;
    }

    public List<PictureURL> getPictureURL ()
    {
        return PictureURL;
    }

    public void setPictureURL (List<PictureURL> PictureURL)
    {
        this.PictureURL = PictureURL;
    }

    public String getTamilContent ()
    {
        return TamilContent;
    }

    public void setTamilContent (String TamilContent)
    {
        this.TamilContent = TamilContent;
    }

    public String getGeoLocation ()
    {
        return GeoLocation;
    }

    public void setGeoLocation (String GeoLocation)
    {
        this.GeoLocation = GeoLocation;
    }

    public String getPlaceName ()
    {
        return PlaceName;
    }

    public void setPlaceName (String PlaceName)
    {
        this.PlaceName = PlaceName;
    }

    public String getTamilTitle ()
    {
        return TamilTitle;
    }

    public void setTamilTitle (String TamilTitle)
    {
        this.TamilTitle = TamilTitle;
    }

    public String getEnglishContent ()
    {
        return EnglishContent;
    }

    public void setEnglishContent (String EnglishContent)
    {
        this.EnglishContent = EnglishContent;
    }

    public String getStoryDate ()
    {
        return StoryDate;
    }

    public void setStoryDate (String StoryDate)
    {
        this.StoryDate = StoryDate;
    }

    public String getStoryID ()
    {
        return StoryID;
    }

    public void setStoryID (String StoryID)
    {
        this.StoryID = StoryID;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [EnglishTitle = "+getEnglishTitle()+", PictureURL = "+PictureURL+"," +
                " TamilContent = "+getTamilContent()+", GeoLocation = "+getGeoLocation()+", PlaceName = "+getPlaceName()+", " +
                "TamilTitle = "+getTamilTitle()+", EnglishContent = "+getEnglishContent()+", StoryDate = "+getStoryDate()+", " +
                "StoryID = "+getStoryID()+"]";
    }
}