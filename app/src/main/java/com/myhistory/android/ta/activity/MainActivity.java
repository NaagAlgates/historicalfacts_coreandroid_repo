package com.myhistory.android.ta.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.net.Uri;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.support.v4.widget.DrawerLayout;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.myhistory.android.ta.R;
import com.myhistory.android.ta.fragment.AboutUsFragment;
import com.myhistory.android.ta.fragment.NavigationDrawerFragment;
import com.myhistory.android.ta.fragment.LoadStoriesFragment;
import com.myhistory.android.ta.gcm.GCMReceiver;
import com.myhistory.android.ta.App;
import com.myhistory.android.ta.utility.AskLanguage;
import com.myhistory.android.ta.utility.GeneralHelper;
import com.myhistory.android.ta.utility.Helper;
import com.myhistory.android.ta.utility.LocaleUtils;
import com.myhistory.android.ta.utility.Logger;

public class MainActivity extends AppCompatActivity
        implements NavigationDrawerFragment.NavigationDrawerCallbacks {

    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private NavigationDrawerFragment mNavigationDrawerFragment;

    /**
     * Used to store the last screen title. For use in {@link #restoreActionBar()}.
     */
    private AdView adViewMainActivity;
    private CharSequence mTitle;
    //String sortTypeFlag;
    private static String TAG="MainActivity";

    public MainActivity() {
        LocaleUtils.updateConfig(this);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        App.get().setLocale(App.get().getLang());
        setContentView(R.layout.activity_main);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            //window.setStatusBarColor(getResources().getColor(R.color.app_actionbar_background));
            window.setStatusBarColor(ContextCompat.getColor(App.get(), R.color.app_actionbar_background));
        }
        GCMReceiver.fetchDeviceToken(this, true,
                new Runnable() {
                    @Override
                    public void run() {
                        //do nothing
                    }
                });
        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);
        mTitle = getTitle();

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));
        GeneralHelper.setBadge(App.get(),0);
        adViewMainActivity = (AdView) findViewById(R.id.adViewMainActivity);
        AdRequest adRequest = new AdRequest.Builder().build();
        adViewMainActivity.loadAd(adRequest);
        adViewMainActivity.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                //Toast.makeText(getApplicationContext(), "Ad is loaded!", Toast.LENGTH_SHORT).show();
                Logger.v(TAG,"Ad loaded");
                adViewMainActivity.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAdClosed() {
                //Toast.makeText(getApplicationContext(), "Ad is closed!", Toast.LENGTH_SHORT).show();
                Logger.v(TAG,"Ad closed");
                adViewMainActivity.setVisibility(View.GONE);
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                //Toast.makeText(getApplicationContext(), "Ad failed to load! error code: " + errorCode, Toast.LENGTH_SHORT).show();
                Logger.v(TAG,"Ad failed to load. Error code : "+errorCode);
                adViewMainActivity.setVisibility(View.GONE);
            }

            @Override
            public void onAdLeftApplication() {
                //Toast.makeText(getApplicationContext(), "Ad left application!", Toast.LENGTH_SHORT).show();
                Logger.v(TAG,"Ad left application");
                adViewMainActivity.setVisibility(View.GONE);
            }

            @Override
            public void onAdOpened() {
                //Toast.makeText(getApplicationContext(), "Ad is opened!", Toast.LENGTH_SHORT).show();
                Logger.v(TAG,"Ad opened");
            }
        });
        /*new RegisterGCM(MainActivity.this).execute();*/

//        Fragment objFragment  = new LoadStoriesFragment();
//        FragmentManager fragmentManager = getSupportFragmentManager();
//        fragmentManager.beginTransaction()
//                .replace(R.id.container, objFragment).commit();
        Intent intent = new Intent("finish_activity");
        sendBroadcast(intent);
    }


    @Override
    public void onNavigationDrawerItemSelected(int position) {
        // update the main content by replacing fragments
        /*FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.container, PlaceholderFragment.newInstance(position + 1))
                .commit();*/
        Fragment objFragment = null;
        Log.v(TAG, "onNavigationDrawerItemSelected");
        SharedPreferences pref = getSharedPreferences(Helper.APP_SHARED_PREF, 0);
        switch(position){
            case 0:
                setSortType(pref.getString(Helper.SORTING_TYPE,"0"));
                objFragment = new LoadStoriesFragment();
                //objFragment = new OnlineFragment();
                break;
            /*case 1:
                objFragment = new OfflineFragment();
                break;*/
            case 1:
                objFragment = new AboutUsFragment();
                break;
            case 2:
                showContactUs();
                //Toast.makeText(this,"Option 4",Toast.LENGTH_SHORT).show();
                break;
            case 3:
                openRateUs();
                //Toast.makeText(this,"Option 5",Toast.LENGTH_SHORT).show();
                break;
            case 4:
                showTermsLayout();
                //Toast.makeText(this,"Option 6",Toast.LENGTH_SHORT).show();
                break;
            case 5:
                showCredits();
                //Toast.makeText(this,"Option 7",Toast.LENGTH_SHORT).show();
                break;
            case 6:
                //objFragment = new DonateFragment();
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(Helper.URL_DONATION));
                startActivity(browserIntent);
                //Toast.makeText(this,"Option 8",Toast.LENGTH_SHORT).show();

                break;
            case 7:
                showAppVersion();
                //Toast.makeText(this,"Option 8",Toast.LENGTH_SHORT).show();
                break;
            default:
                setSortType(pref.getString(Helper.SORTING_TYPE,"0"));
                objFragment = new LoadStoriesFragment();
                break;

        }
        if(objFragment!=null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.container, objFragment).commit();
        }
    }

    /*public void onSectionAttached(int number) {
        switch (number) {
            case 1:
                mTitle = getString(R.string.title_section1);
                break;
            case 2:
                mTitle = getString(R.string.title_section2);
                break;
            case 3:
                mTitle = getString(R.string.title_section3);
                break;
            case 4:
                mTitle = getString(R.string.title_section4);
                break;
            case 5:
                mTitle = getString(R.string.title_section5);
                break;
            case 6:
                mTitle = getString(R.string.title_section6);
                break;
            case 7:
                mTitle = getString(R.string.title_section7);
                break;
        }
    }*/

    public void restoreActionBar() {
        ActionBar actionBar = getSupportActionBar();
        //actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        assert actionBar != null;
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(mTitle);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            getMenuInflater().inflate(R.menu.main, menu);
            restoreActionBar();
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        /*if (id == R.id.action_settings) {
            return true;
        }*/
        Fragment objFragment = null;
        Log.v(TAG, "onOptionsItemSelected");
        switch (id){
            case R.id.menu_section1:
                setSortType("1");
                objFragment = new LoadStoriesFragment();
            break;
            case R.id.menu_section2:
                setSortType("2");
                objFragment = new LoadStoriesFragment();
                break;
            case R.id.menu_section3:
                setSortType("3");
                objFragment = new LoadStoriesFragment();
                break;
            case R.id.menu_section4:
                setSortType("0");
                objFragment = new LoadStoriesFragment();
                break;
            case R.id.menu_section5:
                AskLanguage askLanguage = new AskLanguage(MainActivity.this,false);
                askLanguage.show();
            break;
        }
        if(objFragment!=null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.container, objFragment).commit();
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    /*public static class PlaceholderFragment extends Fragment {
        *//**
         * The fragment argument representing the section number for this
         * fragment.
         *//*
        private static final String ARG_SECTION_NUMBER = "section_number";

        *//**
         * Returns a new instance of this fragment for the given section
         * number.
         *//*
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            //View rootView = inflater.inflate(R.layout.fragment_main, container, false);
            //return rootView;
            return inflater.inflate(R.layout.fragment_main, container, false);
        }
    }*/
    private void openRateUs()
    {
        String appPackageName = App.get().getPackageName();
        Log.i("appPackageName", appPackageName);
        try
        {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
        }
        catch (android.content.ActivityNotFoundException anfe)
        {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + appPackageName)));
        }
    }
    private void showAppVersion()
    {
        String version="null";
        try
        {
            PackageInfo pInfo = getPackageManager().getPackageInfo(App.get().getPackageName(), 0);
            version = pInfo.versionName;
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setMessage(getString(R.string.contentAppVersion)+version)
                .setTitle(getString(R.string.headingAppVersion))
                .setCancelable(false)
                .setIcon(android.R.drawable.radiobutton_on_background)
                .setPositiveButton(getString(R.string.close), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //do things
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }
    private void showCredits()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setMessage(R.string.CreditsText)
                .setTitle("CREDITS")
                .setCancelable(false)
                .setIcon(R.drawable.star_icon)
                .setPositiveButton(getString(R.string.close), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //do things
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }
    public void showTermsLayout()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setMessage(R.string.EULA)
                .setTitle("EULA")
                .setCancelable(false)
                .setIcon(R.drawable.ic_warning)
                .setPositiveButton(getString(R.string.close), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //do things
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }
    private void showContactUs()
    {

        String subject= Helper._contactUsMailSubject;
        String message=Helper._contactUsMailContent;

        String _mobileDetails;
        String _mobileAndroidVersion= GeneralHelper.getAndroidVersion();
        String _mobileName=GeneralHelper.getDeviceName();

        subject = subject.replace("APP_NAME", GeneralHelper.getApplicationName(App.get()));
        _mobileDetails=_mobileAndroidVersion+"\n\n"+_mobileName;

        message = message.replace("MOBILE_DETAILS",_mobileDetails);
        Log.i(TAG,message);

        Intent i = new Intent(Intent.ACTION_SENDTO);
        i.setData(Uri.parse("mailto:"));
        i.putExtra(Intent.EXTRA_EMAIL, Helper.contactMailID);
        i.putExtra(Intent.EXTRA_CC,  Helper._contactMailIDs);
        i.putExtra(Intent.EXTRA_SUBJECT, subject);
        i.putExtra(Intent.EXTRA_TEXT   , message);
        try {
            startActivity(Intent.createChooser(i, "Mail"));
        } catch (android.content.ActivityNotFoundException ex) {
            //getActivity().getApplicationContext().TshowShortToastMessage(mContext, R.string.msgNoMailClientsInstalled);
            Toast.makeText(App.get(),getString(R.string.msgNoMailClientsInstalled),Toast.LENGTH_LONG).show();
        }
    }

    /*public String getSortType() {
        return sortTypeFlag;
    }*/
    public String setSortType(String sortType) {
        SharedPreferences.Editor editor = getSharedPreferences(Helper.APP_SHARED_PREF, MODE_PRIVATE).edit();
        editor.putString(Helper.SORTING_TYPE, sortType);
        editor.apply();
        return sortType;
    }
    /*@Override
    public void onStart() {
        super.onStart();
        //EventBus.getDefault().register(App.get());
    }*/
    @Override
    public void onStop() {
        super.onStop();
        unregisterReceiver( broadcast_receiver);
        Log.d(TAG, "onStop");
        //EventBus.getDefault().unregister(App.get());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy");
        if (adViewMainActivity != null) {
            adViewMainActivity.destroy();
        }
    }

    public void donateUsClicked(View view) {
        Toast.makeText(App.get(),"சேவை விரைவில் இயக்குவிக்கப்படும்!",Toast.LENGTH_LONG).show();
    }
    @Override
    public void onPause() {
        super.onPause();
        Log.d(TAG, "onPause");
        if (adViewMainActivity != null) {
            adViewMainActivity.pause();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "onResume");
        registerReceiver(broadcast_receiver, new IntentFilter("finish_main_activity"));
        if (adViewMainActivity != null) {
            adViewMainActivity.resume();
            Log.d(TAG, "onResume adViewMainActivity");
        }
    }
    BroadcastReceiver broadcast_receiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context arg0, Intent intent) {
            Log.d(TAG, "broadcast_receiever");
            String action = intent.getAction();
            if (action.equals("finish_main_activity")) {
                finish();
                // DO WHATEVER YOU WANT.
            }
        }
    };

    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setTitle(getString(R.string.exit_title))
                .setMessage(getString(R.string.exit_content))
                .setNegativeButton(R.string.no, null)
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface arg0, int arg1) {
                        MainActivity.super.onBackPressed();
                    }
                }).create().show();
    }

}
