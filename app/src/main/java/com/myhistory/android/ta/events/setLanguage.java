package com.myhistory.android.ta.events;

/**
 * Created by NA112077 on 10/27/2016.
 * Last Edited by NA112077 on 10/27/2016
 * Project Name : historicalfacts_coreandroid_repo
 * Package Name : com.myhistory.android.ta.events
 */

public class SetLanguage {
    public String selectedLanguage;
    public boolean isSplash;
    public SetLanguage(String selectedLanguage,boolean isSplash){
        this.selectedLanguage = selectedLanguage;
        this.isSplash = isSplash;
    }
}
