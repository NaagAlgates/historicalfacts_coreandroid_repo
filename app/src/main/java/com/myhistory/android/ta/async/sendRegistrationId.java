package com.myhistory.android.ta.async;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;

import com.myhistory.android.ta.utility.Helper;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import java.io.IOException;

/**
 * Created by Nagaraj.A on 2/29/2016.
 * Last Modified by : Nagaraj.A
 * Package Name : com.myhistory.android.ta.async
 * Project Name : TamilHistoricalFacts
 */
public class sendRegistrationId extends AsyncTask<Void,Void,Void> {
    String gcmID;
    Context context;
    public sendRegistrationId(Context context,String gcmID){
        this.gcmID = gcmID;
        this.context=context;
    }
    @Override
    protected Void doInBackground(Void... params) {
        try {
            hitBackEnd(gcmID);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private  void hitBackEnd(String RegID) {

        try {
            final OkHttpClient client = new OkHttpClient();
            RequestBody formBody = new FormEncodingBuilder()
                    .add("regId", RegID)
                    .build();
            Request request = new Request.Builder()
                    .url(Helper.URL_REGISTER_DEVICE_ID)
                    .post(formBody)
                    .build();

            Response response = client.newCall(request).execute();
            if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);
            else{

                SharedPreferences pref = context.getSharedPreferences(Helper.APP_SHARED_PREF, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = pref.edit();
                editor.putString(Helper.GCM_REG_ID, gcmID);
                editor.apply();
            }

        }  catch (Exception e){
            e.printStackTrace();
        }
    }
}
