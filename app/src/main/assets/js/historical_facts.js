$(document).ready(function () {
    console.log("ready!");
    JSInterface_getStoryLists();	
});

function displayStoryLists(result) {
	closeDialog();
	var langValue = getParameterByName('lang');
	
	console.log("No of Stories " + result.length);
	if (0==result.length) {
        $('#noStories').show();
	} else {
	    $('#noStories').hide();
	}
	var templateData = [];
	
	for (i=0;i<result.length;i++) {
		var imageList = result[i].PictureURL;
        var imageURL;
        var storyTitle;
        var storyContent;
        // Home page Image
        for (var j = 0; j < imageList.length; j++) {
            if (imageList[j].IsHomeImage == 1) {
                imageURL = imageList[j].PictureURL;
                break;
            }
        }
        
        // Story Title
        if (0 == langValue.localeCompare('English')) {
        	console.log("English Title " + result[i].EnglishTitle);
        	storyTitle = result[i].EnglishTitle;
        	storyContent = result[i].EnglishContent;
        } else {
        	console.log("Tamil Title " + result[i].TamilTitle);
        	storyTitle = result[i].TamilTitle;
        	storyContent = result[i].TamilContent;
        }
        
        // Preparing template data with StoryID, language, homeImage URL & Story Title
        templateData.push({
            storyID: result[i].StoryID,
            lang: langValue,
            pictureURL: imageURL,
            storyTitle: storyTitle,
            storyContent : storyContent
        });
	}
	
	var markup = '\
		<div class="panel panel-default">\
			<div class="panel-heading">\
				<strong  style="display:inline;">${storyTitle}</strong><span style="float:right" onclick="JSInterface_deleteStory(${storyID})"><img src="images/delete-26.png" class="img-responsive" /></span>\
			</div>\
			<div class="panel-body">\
				<img src="${pictureURL}" class="img-responsive story-image"/>\
				<div class="story-content-panel text-responsive " hspace="10">\
					<p class="story-content">\
						${storyContent}\
					</p>\
					<button type="button" class="btn btn-default text-responsive story-content-more" onclick="JSInterface_getDetailedStory(${storyID});"> + MORE</button>\
				</div>\
			</div>\
		</div>';
		
	// Compile the template with markup    
    $.template("storyListTemplate", markup);
    
     /* Render the template with the month data and insert
		the rendered HTML under the "storiesList" element */
    $.tmpl("storyListTemplate", templateData).appendTo("#storyList");
    
}

function resetDialog() {
	$("#storyTitle").text('');
	$("#storyContent").text('');
	$('#helpUsTranslate').hide();
	$('#launchMapOpt').hide();	
	$('#images').empty();
	$('#homePicture').attr('src','');	
	
	$('#launchMapOpt').off();
	$('#deleteOpt').off();
	$('#shareOpt').off();
}


/**
 * 
 * @param {Object} storyID
 * @param {Object} lang
 */
function renderDetailedStory(story) {
	console.log("renderDetailedStory() ");
	openDialog();
	resetDialog();
	//var story = JSInterface_getDetailedStory(storyID)[0];
	var lang = getParameterByName('lang');
	console.log(story);
	var pictures = story.PictureURL;
	var title;
	var content;
	if(lang.localeCompare("English") == 0) {		
		$("#storyTitle").text(story.EnglishTitle);
		title = story.EnglishTitle;
		// If story Content for English is 'NONE' then we should show the help us text
		if (story.EnglishContent=='NONE') {
			$("#helpUsTranslate").show();
			$("#storyContent").hide();
			$("#helpUsOpt").on("click", {StoryTitle : story.EnglishTitle}, helpUsByTranslating);
		} else {
			$("#storyContent").show();
			$("#storyContent").text(story.EnglishContent);
			content = story.EnglishContent;			
		}
	} else {
		$("#storyTitle").text(story.TamilTitle);
		$("#storyContent").text(story.TamilContent);
		title = story.TamilTitle;
		content = story.TamilContent;
	}

	
	var homeImageURL;
	
	// Render Image	
	for(var i = 0; i < pictures.length; i++) {
		if(pictures[i].IsHomeImage == 1) {
			homeImageURL = pictures[i].PictureURL;
			$('#homePicture').attr('src', pictures[i].PictureURL);
			$('#homePicture').css({
				'display' : 'block'
			});
		} else {
			$('<img/>', {
				id : 'image[' + i + ']',
				class : "img-responsive",
				src : pictures[i].PictureURL
			}).appendTo('#images')
		}
	}
	
	// Add function to 'Take Me There'
	if (story.GeoLocation != 0) {
		$('#launchMapOpt').show();		
		$('#launchMapOpt').on("click", {GeoLocation: story.GeoLocation}, showMap);
	}
	
	// Sets the proper story ID for download story and share story
	$('#deleteOpt').on("click", {StoryID : story.StoryID}, deleteStory);
	$('#shareOpt').on("click", {StoryTitle : title, StoryContent : content, StoryImage : homeImageURL}, shareTheStory);
}


/**
 * To handle hard back pressed in mobile
 */
function back() {
	if (document.getElementById("storyDialog").style.display == 'none') {
		// Call Android close API
		window.interface.closeWebView();
	} else {
		closeDialog();
	}
}

function deleteStory(evnt) {
	JSInterface_deleteStory(evnt.data.StoryID);	
}

/**
 * Calling the Android native API with longitude and latitude values
 * to show the map app  
 * @param {Object} evnt : from this we can get the map data
 */
function showMap(evnt) {
	console.log("showMap " + evnt.data.GeoLocation);
	// Converting to latitude and longitude
	var location = evnt.data.GeoLocation.split(",");	
	window.interface.MapaCallfrmHTML(location[0],location[1]);
}

/**
 * Calling the Android native API with the story ID and other details which will
 * share the story in either FB, Twitter etc.,
 * @param {Object} evnt : from this we can get StoryID and other details
 */
function shareTheStory(evnt) {
	console.log("shareTheStory Title " + evnt.data.StoryTitle);
	console.log("shareTheStory Content " + evnt.data.StoryContent);
	console.log("shareTheStory Image " + evnt.data.StoryImage);
	// Calling native API
	window.interface.ShareHistoryFunction(evnt.data.StoryTitle + " : Get it on http://goo.gl/CK8mSP", evnt.data.StoryContent, evnt.data.StoryImage);
}

/**
 * Calling the Android native API with the story title to show the email client with the subject as title
 * @param {Object} evnt : from this we can get the story title which we will give to android native
 */
function helpUsByTranslating(evnt) {
	console.log("helpUsByTranslating Story Title " + evnt.data.StoryTitle);
	// Calling native API
	window.interface.SendTranslatedStory(evnt.data.StoryTitle);	
}

function JSInterface_getStoryLists() {
	// Calling Android Native function
	window.interface.getDownloadedStoryLists();
		
}

function JSInterface_takeStoryList(storyListObj) {
	displayStoryLists(storyListObj);
}
/**
 * JS Interface function to get the detailed story from SQLite
 * @param {integer} storyId 
 */
function JSInterface_getDetailedStory(storyId) {
	// Calling Android Native function
	window.interface.getDownloadedStory(storyId);
}

function JSInterface_takeDetailedStory(storyObj) {
	renderDetailedStory(storyObj[0]);	
}

/**
 * JS Interface function to delete the story from SQLite
 * @param {integer} storyId 
 */
function JSInterface_deleteStory(storyId) {
	// Calling Android native function to delete the story
	console.log("JSInterface_deleteStory : storyID " + storyId);
	window.interface.deleteDownloadedStory(storyId);
}

/**
 *   UTILITY API
 * This API extracts the value of the parameter passed and return the same
 * @param {String} name
 */
function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

/**
 *  Function used to show the story detail in a Dialog for detailed reading
 */
function openDialog() {
	// Shows the story div and hides the listItems
	document.getElementById("storyDialog").style.display = 'block';
	document.getElementById("storyList").style.display = 'none';
	// Scroll the body so that itmes shows from the beginning 
	(document.getElementsByTagName("body")[0]).scrollTop = 0;
	
	
}

/**
 *  Function used to close the story dialog and shows back the list items
 */
function closeDialog() {
	// Doing reverse of openDialog
	document.getElementById("storyDialog").style.display = 'none';
	document.getElementById("storyList").style.display = 'block';				
}
